import $ from "jquery";

$(document).ready(function(){
    let ajax_request: JQuery.jqXHR | null = null;
    let search_results_div = $("#gallery-search-results");
    let form_query = $("#gallery-search-form-query")

    history.replaceState(
        {"html": search_results_div.html(), "query": form_query.val()}, ""
    );
    form_query.keyup(function () {
        let query = <string>form_query.val();
        if (query) {
            if (ajax_request !== null)
                ajax_request.abort();
            ajax_request = $.ajax({
                type: "GET",
                url: "/api/gallery/search",
                data: {
                    "query": query
                },
                dataType: "html",
                success: function (data: string) {
                    ajax_request = null;
                    search_results_div.html(data);
                    window.history.pushState(
                        {"html": data, "query": query}, "", `/gallery/search?query=${encodeURIComponent(query)}`
                    )
                }
            });
        }
    });
    $(window).on("popstate", function (event) {
        let original_event = <PopStateEvent>event.originalEvent;
        if (original_event.state) {
            search_results_div.html(original_event.state.html);
            form_query.val(original_event.state.query);
        }
    });
});
