import $ from "jquery";
import "jquery-ui/ui/widgets/sortable";
import "jquery-ui/ui/widgets/tooltip";

const AVAILABLE_LANGUAGES = new Map([
    ["python", "Python"],
    ["javascript", "JavaScript"],
    ["csharp", "C#"],
    ["java", "Java"],
    ["rust", "Rust"],
    ["go", "Go"],
])

function createLanguageNode(value: string, friendly_name: string) {
    let li_node = document.createElement("li");
    li_node.className = "ui-state-default";
    li_node.dataset.value = value;
    let span_node = document.createElement("span");
    span_node.className = "ui-icon ui-icon-arrow-4";
    li_node.appendChild(span_node);
    li_node.appendChild(document.createTextNode(friendly_name));

    return li_node;
}

function restoreContactFormData() {
    const raw_json = window.sessionStorage.getItem("contact-form");
    if (raw_json) {
        const saved_data = JSON.parse(raw_json);
        for (const field_data of saved_data) {
            let field = <HTMLInputElement | HTMLTextAreaElement>document.getElementById(
                `contact-form-${field_data.name}`
            );
            if (field instanceof HTMLInputElement && field.type == "checkbox") {
                field.checked = field_data.value == "on";
            } else {
                field.value = field_data.value;
            }
        }
    }
    initAndRestoreContactFormLanguages();
}

function initAndRestoreContactFormLanguages() {
    const unknown_languages_ul = document.getElementById("contact-form-unknown-languages-ul");
    const preferred_languages_ul = document.getElementById("contact-form-preferred-languages-ul");
    const preferred_languages_input = <HTMLInputElement>document.getElementById(
        "contact-form-preferred-languages"
    );

    const selected_languages = preferred_languages_input.value.split(",");
    for (const value of selected_languages) {
        const friendly_name = AVAILABLE_LANGUAGES.get(value);
        if (friendly_name !== undefined) {
            const li_node = createLanguageNode(value, friendly_name);
            preferred_languages_ul.appendChild(li_node);
        }
    }
    for (const [value, friendly_name] of AVAILABLE_LANGUAGES) {
        if (!selected_languages.includes(value)) {
            const li_node = createLanguageNode(value, friendly_name);
            unknown_languages_ul.appendChild(li_node);
        }
    }
}

function initContactFormLanguagesSortable() {
    const unknown_languages_ul = $("#contact-form-unknown-languages-ul");
    const preferred_languages_ul = $("#contact-form-preferred-languages-ul");
    const preferred_languages_input = <HTMLInputElement>document.getElementById(
        "contact-form-preferred-languages"
    );

    $(".contact-form-languages").sortable({
        connectWith: ".contact-form-languages"
    });
    preferred_languages_ul.on("sortupdate", function (event, ui) {
        let preffered_languages = preferred_languages_ul.sortable(
            "toArray", {attribute: "data-value"}
        );
        preferred_languages_input.value = preffered_languages.join(",");
        let fake_event = new Event("change");
        preferred_languages_input.dispatchEvent(fake_event);
    });

    const sortables = document.querySelectorAll<HTMLElement>(".form-sortable");
    for (let node of sortables) {
        node.style.display = "block";
    }
}

function initContactFormAutoSave() {
    const contact_form = $("#contact-form");
    contact_form.find(":input").on("change keyup", function() {
        window.sessionStorage.setItem(
            "contact-form", JSON.stringify(contact_form.serializeArray())
        );
    });
}

$(document).ready(function(){
    restoreContactFormData();
    initContactFormLanguagesSortable();
    initContactFormAutoSave();
    $(document).tooltip();
});
