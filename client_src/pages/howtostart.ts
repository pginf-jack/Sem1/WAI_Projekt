import $ from "jquery";
import "jquery-ui/ui/widgets/tooltip";

const ID_PREFIX_LENGTH = "challenge-list-".length

function restoreChallengeListData() {
    const raw_json = window.localStorage.getItem("challenge-list");
    if (raw_json) {
        const saved_data = JSON.parse(raw_json);
        for (const [idx, state] of saved_data) {
            let field = <HTMLInputElement>document.getElementById(
                `challenge-list-${idx}`
            );
            if (field !== null) {
                field.checked = state;
            }
        }
    }
}

function generateCheckboxStateList(checkbox_list: NodeListOf<HTMLInputElement>) {
    let checkbox_states: [number, boolean][] = [];
    for (let checkbox of checkbox_list) {
        checkbox_states.push(
            [parseInt(checkbox.id.slice(ID_PREFIX_LENGTH), 10), checkbox.checked]
        );
    }
    return checkbox_states;
}

function initChallengeListAutoSave() {
    const challenge_list = document.getElementById("challenge-list");
    let checkbox_list = challenge_list.querySelectorAll<HTMLInputElement>("input[type=checkbox]");
    for (let checkbox of checkbox_list) {
        checkbox.addEventListener("change", function (event) {
            window.localStorage.setItem(
                "challenge-list", JSON.stringify(generateCheckboxStateList(checkbox_list))
            );
        });
    }
}

function initChallengeList() {
    const challenge_list = document.getElementById("challenge-list");
    challenge_list.className = "js-enabled"
}

$(document).ready(function(){
    restoreChallengeListData();
    initChallengeListAutoSave();
    initChallengeList();
    $(document).tooltip();
});
