import $ from "jquery";
import "jquery-ui/ui/widgets/tooltip";
import "slick-carousel";

$(document).ready(function(){
    $("#companies-carousel").slick({
        infinite: true,
        slidesToShow: 4,
        dots: true,
        autoplay: true,
        speed: 600,
        responsive: [
            {
              breakpoint: 1280,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
              }
            },
            {
              breakpoint: 968,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
              }
            },
        ]
    });
    $(document).tooltip();
});
