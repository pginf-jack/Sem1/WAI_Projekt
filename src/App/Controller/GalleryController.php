<?php
namespace App\Controller;

use App\Exception\HTTPException;
use App\Exception\IdsNotFoundException;
use App\Exception\IllegalFileTypeValidationException;
use App\Exception\MaxFileSizeExceededValidationException;
use App\Exception\RequiredInputMissingValidationException;
use App\Exception\ValidationException;
use App\Exception\RuntimeException;
use App\FormModel\ImageFormModel;
use App\FormModel\ImageSearchFormModel;
use App\Lib\AppState;
use App\Lib\BaseController;
use App\Lib\User;
use App\Lib\Utils;
use App\Lib\View;
use App\Model\ImageCollectionModel;
use App\Model\ImagePage;
use App\Model\RememberedImagesCollectionModel;

use MongoDB\BSON\ObjectId;
use MongoDB\Driver\Exception as MongoDBDriverException;

require_once Utils::SRC_DIR . '/App/Lib/constants.php';
use const App\Lib\HTTP_METHOD_GET;

final class GalleryController extends BaseController {
    public function showUploadForm() {
        $this->showUploadFormImpl(
            new ImageFormModel(['allow_private' => $this->app_state->user !== null])
        );
    }

    public function handleUploadForm() {
        $form_model = ImageFormModel::constructWithData(
            $this->request->form_data,
            ['allow_private' => $this->app_state->user !== null]
        );
        try {
            $this->validateFilesAreUploaded();
            $this->validateUploadForm($form_model);
            $this->addImage($form_model);
        } catch (HTTPException $e) {
            $this->handleHTTPException($e);
            $this->showUploadFormImpl($form_model);
            return;
        }

        $this->response->redirectWithMessage(
            'Zdjęcie zostało dodane.',
            static::class,
            'showUploadForm'
        );
    }

    private function showUploadFormImpl(ImageFormModel $form_model) {
        if (!$form_model->inputs['author']->getValue() && $this->app_state->user)
            $form_model->inputs['author']->setValue($this->app_state->user->username);
        $this->prepareModel($this->getMenuEntry());
        $this->response->data['form'] = $form_model;
        $this->setView('gallery/upload_form');
    }

    private function validateFilesAreUploaded() {
        if (!isset($this->request->files['file']))
            throw new HTTPException('Wymagane pole (Zdjęcie) nie zostało podane.', 400);

        $files = $this->request->files['file'];

        foreach ($files as $file) {
            switch ($file->error) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw HTTPException::fromStatusCode(413);
                case UPLOAD_ERR_PARTIAL:
                case UPLOAD_ERR_NO_FILE:
                    throw HTTPException::fromStatusCode(400);
                case UPLOAD_ERR_NO_TMP_DIR:
                case UPLOAD_ERR_CANT_WRITE:
                case UPLOAD_ERR_EXTENSION:
                default:
                    throw HTTPException::fromStatusCode(500);
            }

            // This should technically never happen, but let's be extra safe
            if (!is_uploaded_file($file->tmp_name))
                throw HTTPException::fromStatusCode(400);
        }
    }

    private function validateUploadForm(ImageFormModel $form_model) {
        try {
            $form_model->validate();
        } catch (IllegalFileTypeValidationException $e) {
            throw new HTTPException($e->getMessage(), 415, $e);
        } catch (MaxFileSizeExceededValidationException $e) {
            throw new HTTPException($e->getMessage(), 413, $e);
        } catch (ValidationException $e) {
            throw new HTTPException($e->getMessage(), 400, $e);
        }
    }

    private function addImage(ImageFormModel $form_model) {
        $image_collection = new ImageCollectionModel();
        try {
            return $image_collection->add(
                $form_model->inputs['file']->getTempFileName(),
                $form_model->inputs['file']->getFileExtension(),
                $form_model->inputs['watermark']->getValue(),
                $form_model->inputs['title']->getValue(),
                $form_model->inputs['author']->getValue(),
                $this->app_state->user,
                $form_model->isPublic()
            );
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        } catch (RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    public function showUserImages(string $page_number = '1') {
        $this->response->data['pager_base_url'] = '/gallery/me[PAGE_PLACEHOLDER]';
        $this->showImagesImpl('gallery/user_images', $page_number, $this->app_state->user, false);
    }

    public function showImages(string $page_number = '1') {
        $this->response->data['pager_base_url'] = '/gallery[PAGE_PLACEHOLDER]';
        $this->showImagesImpl('gallery/images', $page_number, $this->app_state->user);
    }

    private function showImagesImpl(
        string $view_name, string $page_number = '1', User $user = null, bool $show_others = true
    ) {
        $this->prepareModel($this->getMenuEntry());
        try {
            $page_number = $this->getPageNumber($page_number);
            $this->response->data['image_page'] = $this->getImages(
                $page_number, $user, $show_others
            );
            $this->response->data['selected_images_ids'] = $this->getSelectedImageIds($user);
        } catch (HTTPException $e) {
            $this->handleHTTPException($e, 'message');
            if ($e->getCode() >= 500)
                $this->setView('errors/server_error');
            else
                $this->setView('errors/custom_message');
            return;
        }
        $this->setView($view_name);
    }

    private function getPageNumber(string $page_number) {
        if (!preg_match('/^[1-9]\d*$/', $page_number))
            throw new HTTPException('Parametr w adresie strony jest niepoprawny.', 400);
        return intval($page_number);
    }

    private function getImages(
        int $page_number, User $user = null, bool $show_others = true
    ): ImagePage {
        $image_collection = new ImageCollectionModel();
        try {
            return $image_collection->getPage($page_number, 5, $user, $show_others);
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    public function handleSearchFormAPI(string $page_number = '1', bool $include_base = false) {
        $this->prepareModel($this->getMenuEntry());
        $form_model = ImageSearchFormModel::constructWithData(
            $this->request->query_params
        );
        $this->response->data['form'] = $form_model;
        try {
            $page_number = $this->getPageNumber($page_number);
            $this->validateSearchForm($form_model);
            $this->response->data['image_page'] = $this->getImagesWithMatchingTitle(
                $form_model, $page_number
            );
            $this->response->data['selected_images_ids'] = $this->getSelectedImageIds($this->app_state->user);
        } catch (HTTPException $e) {
            $this->handleHTTPException(
                new HTTPException($e->getMessage(), 400, $e), 'error_message'
            );
        }

        $this->response->data['pager_base_url'] = (
            '/gallery/search[PAGE_PLACEHOLDER]?query=' . urlencode($form_model->inputs['query']->getValue())
        );
        $this->setView('gallery/search', $include_base);
    }

    public function showSearchForm(string $page_number = '1') {
        $this->handleSearchFormAPI($page_number, true);
    }

    private function validateSearchForm(ImageSearchFormModel $form_model) {
        try {
            $form_model->validate();
        } catch (RequiredInputMissingValidationException $e) {
            throw new HTTPException('', 400, $e);
        } catch (ValidationException $e) {
            throw new HTTPException($e->getMessage(), 400, $e);
        }
    }

    private function getImagesWithMatchingTitle(
        ImageSearchFormModel $form_model, int $page_number
    ) {
        $query = $form_model->inputs['query']->getValue();
        if ($query === null)
            return null;

        $image_collection = new ImageCollectionModel();
        try {
            return $image_collection->searchByTitle($query, $page_number, 5, $this->app_state->user);
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    private function getSelectedImageIds(User $user = null): array {
        $remembered_images_collection = new RememberedImagesCollectionModel();
        try {
            return $remembered_images_collection->getImageIds($user);
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    public function showRememberedImages() {
        $this->prepareModel($this->getMenuEntry());
        try {
            $this->response->data['image_page'] = $this->getRememberedImages();
        } catch (HTTPException $e) {
            $this->handleHTTPException($e, 'message');
            $this->setView('errors/server_error');
            return;
        }

        $this->response->data['pager_base_url'] = '/images/remembered[PAGE_PLACEHOLDER]';
        $this->setView('gallery/remembered_images');
    }

    private function getRememberedImages(): ImagePage {
        $remembered_images_collection = new RememberedImagesCollectionModel();
        try {
            return $remembered_images_collection->getPage($this->app_state->user);
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    public function addOrRemoveRememberedImages() {
        try {
            $this->addOrRemoveRememberedImagesRunOperation();
        } catch (HTTPException $e) {
            $this->handleHTTPException($e);
            $this->showRememberedImages();
            return;
        }
    }

    private function addOrRemoveRememberedImagesRunOperation() {
        if (!isset($this->request->form_data['operation']))
            throw HTTPException::fromStatusCode(400);

        $operation = $this->request->form_data['operation'];

        switch ($operation) {
            case 'add':
                $this->addRememberedImages();
                break;
            case 'remove':
                $this->removeRememberedImages();
                break;
            default:
                throw HTTPException::fromStatusCode(400);
        }
    }

    private function addRememberedImages() {
        $ids = $this->getSelectedIds();
        $this->addSelectedIds($ids);

        try {
            $this->response->redirectBackWithMessage('Wybrane zdjęcia zostały zapamiętane.');
        } catch (RouteNotFoundException $e) {
            $this->addUserMessage($this->response->data['redirect_message']);
            unset($this->response->data['redirect_message']);
            $this->showRememberedImages();
        }
    }

    private function removeRememberedImages() {
        $ids = $this->getSelectedIds();
        $this->removeSelectedIds($ids);

        $this->response->redirectWithMessage(
            'Wybrane zdjęcia zostały usunięte z zapamiętanych.',
            static::class,
            'showRememberedImages'
        );
    }

    private function getSelectedIds(): array {
        if (!isset($this->request->form_data['selected_images'])) {
            throw HTTPException::fromStatusCode(400);
        }

        $selected_images = $this->request->form_data['selected_images'];
        if (!is_array($selected_images)) {
            throw HTTPException::fromStatusCode(400);
        }

        $ids = [];
        foreach ($selected_images as $image_id) {
            try {
                $ids[] = new ObjectId($image_id);
            } catch (MongoDBDriverException\InvalidArgumentException $e) {
                throw HTTPException::fromStatusCode(400, $e);
            }
        }
        return $ids;
    }

    private function addSelectedIds(array $ids) {
        $remembered_images_collection = new RememberedImagesCollectionModel();
        try {
            $remembered_images_collection->addImages($ids, $this->app_state->user);
        } catch (IdsNotFoundException $e) {
            throw HTTPException::fromStatusCode(400, $e);
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    private function removeSelectedIds(array $ids) {
        $remembered_images_collection = new RememberedImagesCollectionModel();
        try {
            $remembered_images_collection->removeImages($ids, $this->app_state->user);
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    public static function loginListener(AppState $app_state) {
        $remembered_images_collection = new RememberedImagesCollectionModel();
        $remembered_images_collection->transferImageIdsFromSessionToUser($app_state->user);
    }

    private function getMenuEntry() {
        return ltrim($this->router->routeTo(HTTP_METHOD_GET, static::class, 'showImages'), '/');
    }
}
