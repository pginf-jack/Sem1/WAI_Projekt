<?php
namespace App\Controller;

use App\Exception\HTTPException;
use App\Lib\BaseController;
use App\Lib\Utils;
use App\Lib\View;

final class StaticPageController extends BaseController {
    private static $available_pages;

    private function getAvailablePages(): array {
        if (is_null(self::$available_pages)) {
            $orig_cwd = getcwd();
            chdir(Utils::SRC_DIR . '/App/View/pages');
            self::$available_pages = array_map(
                function (string $element) {
                    return basename($element, '.php');
                },
                glob('*.php')
            );
        }
        return self::$available_pages;
    }

    protected function prepareModel(string $menu_current = null) {
        if ($menu_current === 'index')
            $menu_current = '';
        parent::prepareModel($menu_current);
    }

    public function showPage(string $page_name = '') {
        if (!$page_name)
            $page_name = 'index';

        if (!in_array($page_name, $this->getAvailablePages())) {
            $this->notFound();
            return;
        }

        $this->prepareModel($page_name);
        $this->setView('pages/' . $page_name);
    }

    public function notFound() {
        $this->response->status_code = 404;
        $this->prepareModel(null);
        $this->setView('errors/not_found');
    }

    public function forbidden(string $message) {
        $this->response->status_code = 403;
        $this->prepareModel(null);
        $this->response->data['message'] = $message;
        $this->setView('errors/forbidden');
    }

    public function serverError(HTTPException $e) {
        $this->handleHTTPException($e, 'message');
        $this->prepareModel(null);
        $this->setView('errors/server_error');
    }
}
