<?php
namespace App\Controller;

use MongoDB\Driver\Exception as MongoDBDriverException;

use App\Exception\HTTPException;
use App\Exception\PasswordInvalidException;
use App\Exception\UserAlreadyExistsException;
use App\Exception\UserNotFoundException;
use App\Exception\ValidationException;
use App\FormModel\LoginFormModel;
use App\FormModel\RegistrationFormModel;
use App\Lib\BaseController;
use App\Model\UserCollectionModel;

final class UserController extends BaseController {
    public function showRegistrationForm() {
        $this->showRegistrationFormImpl(new RegistrationFormModel());
    }

    private function showRegistrationFormImpl(RegistrationFormModel $form_model) {
        $this->prepareModel('user/login');
        $this->response->data['form'] = $form_model;
        $this->setView('user/registration_form');
    }

    public function handleRegistrationForm() {
        $form_model = RegistrationFormModel::constructWithData($this->request->form_data);
        try {
            $this->validateRegistrationForm($form_model);
            $this->addUser($form_model);
        } catch (HTTPException $e) {
            $this->handleHTTPException($e);
            $this->showRegistrationFormImpl($form_model);
            return;
        }

        $this->response->redirectWithMessage(
            'Twoje konto zostało zarejestrowane.',
            static::class,
            'showLoginForm'
        );
    }

    private function validateRegistrationForm(RegistrationFormModel $form_model) {
        try {
            $form_model->validate();
        } catch (ValidationException $e) {
            throw new HTTPException($e->getMessage(), 400, $e);
        }
    }

    private function addUser(RegistrationFormModel $form_model) {
        $user_collection = new UserCollectionModel();
        try {
            return $user_collection->add(
                $form_model->inputs['email']->getValue(),
                $form_model->inputs['username']->getValue(),
                $form_model->inputs['password']->getValue()
            );
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        } catch (UserAlreadyExistsException $e) {
            throw new HTTPException('Podany login lub email jest już zajęty.', 400);
        } catch (RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    public function showLoginForm() {
        $this->showLoginFormImpl(new LoginFormModel());
    }

    private function showLoginFormImpl(LoginFormModel $form_model) {
        $this->prepareModel('user/login');
        $this->response->data['form'] = $form_model;
        $this->setView('user/login_form');
    }

    public function handleLoginForm() {
        $form_model = LoginFormModel::constructWithData($this->request->form_data);
        try {
            $this->validateLoginForm($form_model);
            $this->loginUser($form_model);
        } catch (HTTPException $e) {
            $this->handleHTTPException($e);
            $this->showLoginFormImpl($form_model);
            return;
        }

        $this->response->redirectWithMessage(
            'Zalogowano.',
            static::class,
            'showProfile'
        );
    }

    private function validateLoginForm(LoginFormModel $form_model) {
        try {
            $form_model->validate();
        } catch (ValidationException $e) {
            throw new HTTPException($e->getMessage(), 401, $e);
        }
    }

    private function loginUser(LoginFormModel $form_model) {
        try {
            return $this->app_state->login(
                $form_model->inputs['username']->getValue(),
                $form_model->inputs['password']->getValue()
            );
        } catch (MongoDBDriverException\ConnectionException $e) {
            throw HTTPException::fromStatusCode(503, $e);
        } catch (MongoDBDriverException\RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        } catch (UserNotFoundException $e) {
            throw new HTTPException('Użytkownik z podanym loginem nie istnieje.', 401);
        } catch (PasswordInvalidException $e) {
            throw new HTTPException('Nieprawidłowe hasło.', 401);
        } catch (RuntimeException $e) {
            throw HTTPException::fromStatusCode(500, $e);
        }
    }

    public function showProfile() {
        $this->prepareModel('user/profile');
        $this->setView('user/profile');
    }

    public function handleLogout() {
        $this->app_state->logout();
        $this->response->redirectWithMessage(
            'Wylogowano.',
            static::class,
            'showLoginForm'
        );
    }
}
