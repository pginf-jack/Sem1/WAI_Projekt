<?php
namespace App\Exception;

class HTTPException extends RuntimeException {
    const ERROR_MESSAGES = [
        400 => 'Serwer otrzymał źle sformułowane żądanie.',
        413 => 'Wybrany plik przekracza dozwolony limit wielkości.',
        415 => 'Wybrany plik jest w niedozwolonym formacie.',
        500 => 'Serwer nie mógł obsłużyć żądania.',
        503 => 'Serwer nie mógł obsłużyć żądania. Spróbuj ponownie później.',
    ];

    public static function fromStatusCode(
        int $status_code, \Throwable $previous = null
    ): HTTPException {
        return new HTTPException(
            self::ERROR_MESSAGES[$status_code], $status_code, $previous
        );
    }
}
