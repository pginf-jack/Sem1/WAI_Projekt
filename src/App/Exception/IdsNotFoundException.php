<?php
namespace App\Exception;

class IdsNotFoundException extends RuntimeException {
    public $ids;

    public function __construct(array $ids) {
        parent::__construct('Ids not found: ' . implode(', ', $ids));
        $this->ids = $ids;
    }
}
