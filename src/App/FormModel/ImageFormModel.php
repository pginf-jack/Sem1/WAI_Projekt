<?php
namespace App\FormModel;

use App\Lib\Form\FormModel;
use App\Lib\Form\Validator\FileValidator;
use App\Lib\Form\Validator\StringLengthValidator;

final class ImageFormModel extends FormModel {
    public $allow_private = false;

    public function __construct(array $options = []) {
        parent::__construct($options);
        if (isset($options['allow_private']))
            $this->allow_private = $options['allow_private'];

        $this->setName('gallery-upload');
        $this->addTextInput([
            'name' => 'title',
            'title' => 'Tytuł',
            'placeholder' => 'Tatry',
            'required' => true,
            'validators' => [
                new StringLengthValidator(['min' => 1, 'max' => 100]),
            ],
        ]);
        $this->addTextInput([
            'name' => 'author',
            'title' => 'Autor',
            'placeholder' => 'Jan Kowalski',
            'required' => true,
            'validators' => [
                new StringLengthValidator(['min' => 1, 'max' => 100]),
            ],
        ]);
        $this->addTextInput([
            'name' => 'watermark',
            'title' => 'Znak wodny',
            'placeholder' => '(c) Jan Kowalski',
            'required' => true,
            'validators' => [
                new StringLengthValidator(['min' => 1, 'max' => 100]),
            ],
        ]);
        $this->addFileInput([
            'name' => 'file',
            'title' => 'Zdjęcie',
            'help' => 'maksymalny rozmiar: 1MB, dozwolone formaty: PNG, JPG',
            'placeholder' => '(c) Jan Kowalski',
            'required' => true,
            'validators' => [
                new FileValidator([
                    'allowed_types' => ['image/jpeg', 'image/png'],
                    'max_size' => 1024*1024,
                ]),
            ],
        ]);
        if ($this->allow_private)
            $this->addRadioGroupInput([
                'name' => 'public',
                'title' => 'Prywatność',
                'required' => true,
                'options' => [
                    ['title' => 'publiczne', 'value' => '1'],
                    ['title' => 'prywatne', 'value' => '0'],
                ],
                'default' => 0,
            ]);
    }

    public function isPublic() {
        if (!$this->allow_private)
            return true;
        return $this->inputs['public']->getValue() == '1';
    }
}
