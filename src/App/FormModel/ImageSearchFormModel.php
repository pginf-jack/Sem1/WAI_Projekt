<?php
namespace App\FormModel;

use App\Lib\Form\FormModel;

final class ImageSearchFormModel extends FormModel {
    public $allow_private = false;

    public function __construct(array $options = []) {
        parent::__construct($options);
        $this->setName('gallery-search');
        $this->setMethod('get');
        $this->addTextInput([
            'name' => 'query',
            'title' => 'Tytuł',
            'placeholder' => 'Tatry',
            'required' => true
        ]);
    }
}
