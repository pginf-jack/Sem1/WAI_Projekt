<?php
namespace App\FormModel;

use App\Lib\Form\FormModel;
use App\Lib\Form\Validator\FileValidator;
use App\Lib\Form\Validator\IdenticalInputValidator;
use App\Lib\Form\Validator\StringLengthValidator;
use App\Lib\Form\Validator\StringRegexValidator;
use App\Model\UserCollectionModel;

final class LoginFormModel extends FormModel {
    public function __construct(array $options = []) {
        parent::__construct($options);
        $this->setName('login');
        $this->addTextInput([
            'name' => 'username',
            'title' => 'Login',
            'placeholder' => 'user123',
            'required' => true,
        ]);
        $this->addPasswordInput([
            'name' => 'password',
            'title' => 'Hasło',
            'required' => true,
        ]);
    }
}
