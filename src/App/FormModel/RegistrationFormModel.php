<?php
namespace App\FormModel;

use App\Lib\Form\FormModel;
use App\Lib\Form\Validator\FileValidator;
use App\Lib\Form\Validator\IdenticalInputValidator;
use App\Lib\Form\Validator\StringLengthValidator;
use App\Lib\Form\Validator\StringRegexValidator;
use App\Model\UserCollectionModel;

final class RegistrationFormModel extends FormModel {
    public function __construct(array $options = []) {
        parent::__construct($options);
        $this->setName('registration');
        $this->addEmailInput([
            'name' => 'email',
            'title' => 'Email',
            'placeholder' => 'user@example.com',
            'required' => true,
        ]);
        $this->addTextInput([
            'name' => 'username',
            'title' => 'Login',
            'placeholder' => 'user123',
            'required' => true,
            'validators' => [
                new StringRegexValidator([
                    'pattern' => UserCollectionModel::USERNAME_PATTERN,
                    'pattern_help' => (
                        'Login może zawierać jedynie litery, cyfry, dywizy (-)'
                        . ' i znaki podkreślenia (_), i musi mieć długość 4-32 znaki.'
                    )
                ]),
            ],
        ]);
        $this->addPasswordInput([
            'name' => 'password',
            'title' => 'Hasło',
            'required' => true,
            'validators' => [
                new StringLengthValidator(['min' => 6]),
            ],
        ]);
        $this->addPasswordInput([
            'name' => 'repeated_password',
            'title' => 'Powtórz hasło',
            'required' => true,
            'validators' => [
                new IdenticalInputValidator(['input_name' => 'password']),
            ],
        ]);
    }
}
