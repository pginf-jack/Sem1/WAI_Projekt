<?php
namespace App\Lib;

use App\Exception\ForbiddenException;

final class ACLRules {
    const ROLE_GUEST = 0;
    const ROLE_USER = 1;
    const ROLE_NAMES = [
        self::ROLE_GUEST => 'Gość',
        self::ROLE_USER => 'Użytkownik',
    ];

    private $default_allowed_roles;
    private $rules = [];

    public function __construct(array $default_allowed_roles) {
        $this->default_allowed_roles = $default_allowed_roles;
    }

    public function setRouteRules(string $route_path, array $methods, array $allowed_roles) {
        if (!isset($this->rules[$route_path]))
            $this->rules[$route_path] = [];

        foreach ($methods as $method)
            $this->rules[$route_path][$method] = $allowed_roles;
    }

    public function validateRoutesExist(Router $router) {
        foreach ($this->rules as $route_path => $route_rules)
            foreach (array_keys($route_rules) as $method)
                if (!$router->hasPath($method, $route_path))
                    throw new \DomainException('ACL rules reference route that does not exist.');
    }

    public function getAllowedRolesByRoute(Route $route) {
        return $this->getAllowedRolesByRoutePath($route->http_method, $route->path);
    }

    private function getAllowedRolesByRoutePath(string $http_method, string $path) {
        if (!isset($this->rules[$path]))
            return $this->default_allowed_roles;
        if (!isset($this->rules[$path][$http_method]))
            return $this->default_allowed_roles;
        return $this->rules[$path][$http_method];
    }

    public function canRunRoutePath(string $http_method, string $path, User $user = null): bool {
        $allowed_roles = $this->getAllowedRolesByRoutePath($http_method, $path);
        $role = $this->getRoleFromUser($user);
        return in_array($role, $allowed_roles);
    }

    public function validatePermissions(Route $route, User $user = null) {
        $allowed_roles = $this->getAllowedRolesByRoute($route);
        $role = $this->getRoleFromUser($user);
        if (!in_array($role, $allowed_roles))
            throw new ForbiddenException(
                'Nie masz uprawnień do tej strony. Strona jest dostępna dla ról: '
                . $this->getFormattedListOfAllowedRoles($allowed_roles)
            );
    }

    private function getRoleFromUser(User $user = null) {
        if ($user === null)
            return self::ROLE_GUEST;
        else
            return $user->getRole();
    }

    public static function getFormattedListOfAllowedRoles(array $allowed_roles) {
        $role_names = [];
        foreach ($allowed_roles as $role)
            $role_names[] = static::ROLE_NAMES[$role];
        return implode(', ', $role_names);
    }
}
