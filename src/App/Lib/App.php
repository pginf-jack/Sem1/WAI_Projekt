<?php
namespace App\Lib;

use MongoDB\Driver\Exception as MongoDBDriverException;

use App\Controller\StaticPageController;
use App\Exception\ForbiddenException;
use App\Exception\HTTPException;
use App\Exception\RouteNotFoundException;
use App\Exception\UserNotFoundException;

final class App {
    protected $router;
    protected $acl;
    protected $app_state;
    protected $dispatcher;

    public function __construct(Router $router, ACLRules $acl, EventDispatcher $dispatcher) {
        $this->router = $router;
        $router->setACLRules($acl);
        $this->acl = $acl;
        $this->dispatcher = $dispatcher;
    }

    public function run() {
        $old_values = [];
        try {
            // Normally I would enable this in php.ini, but I'm gonna assume I shouldn't deviate
            // from what they told me to do in the VM setup instructions.
            $old_values['session.use_strict_mode'] = ini_set('session.use_strict_mode', '1');
            $this->runImpl();
        } finally {
            session_write_close();
            // restore old values in case something else is ran after run()
            foreach ($old_values as $option => $value)
                ini_set($option, $value);
        }
    }

    public function runImpl() {
        session_start();
        $app_state = new AppState($this->acl, $this->dispatcher);
        $response = new Response($app_state, $this->router);

        try {
            $app_state->prepare();
        } catch (MongoDBDriverException\ConnectionException $e) {
            $controller = new StaticPageController($response);
            $controller->serverError(HTTPException::fromStatusCode(503, $e));
        } catch (MongoDBDriverException\RuntimeException $e) {
            $controller = new StaticPageController($response);
            $controller->serverError(HTTPException::fromStatusCode(500, $e));
        } catch (UserNotFoundException $e) {
            session_destroy();
            session_start();
            $response->addUserMessage(
                'Your session is no longer valid and you have been logged out.'
            );
        }

        ob_start();
        try {
            $this->router->dispatch($response);
        } catch (RouteNotFoundException $e) {
            $controller = new StaticPageController($response);
            $controller->notFound();
        } catch (ForbiddenException $e) {
            $controller = new StaticPageController($response);
            $controller->forbidden($e->getMessage());
        }
        $response->body = ob_get_contents();
        ob_end_clean();

        $response->send();
    }
}
