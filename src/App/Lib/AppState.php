<?php
namespace App\Lib;

use App\Model\UserCollectionModel;

final class AppState {
    private $acl;

    public $dispatcher;
    public $request;
    public $user = null;

    public function __construct(ACLRules $acl, EventDispatcher $dispatcher) {
        $this->request = new Request();
        $this->acl = $acl;
        $this->dispatcher = $dispatcher;
    }

    public function login(string $username, string $password) {
        $user_collection = new UserCollectionModel();
        $this->user = $user_collection->getByLogin($username, $password);
        session_regenerate_id();
        $_SESSION['user_id'] = $this->user->id;
        $this->dispatcher->dispatch('login', $this);
    }

    public function logout() {
        unset($this->user);
        $this->user = null;
        session_destroy();
        session_start();
    }

    public function prepare() {
        $this->user = User::constructFromSessionData();
    }

    public function canRunRoutePath(string $http_method, string $path) {
        return $this->acl->canRunRoutePath($http_method, $path, $this->user);
    }
}
