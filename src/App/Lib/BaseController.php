<?php
namespace App\Lib;

use App\Exception\HTTPException;

class BaseController {
    private static $menu;

    public $app_state;
    public $request;
    public $response;
    public $router;

    public function __construct(Response $response) {
        $this->response = $response;
        $this->app_state = $response->app_state;
        $this->router = $response->router;
        $this->request = $this->app_state->request;
    }

    private function getMenu() {
        if (is_null(self::$menu))
            self::$menu = require Utils::SRC_DIR . '/menu.php';
        return self::$menu;
    }

    private function getAndClearRedirectMessage() {
        if (!isset($_SESSION['redirect_message']))
            return null;
        $redirect_message = $_SESSION['redirect_message'];
        unset($_SESSION['redirect_message']);
        return $redirect_message;
    }

    protected function prepareModel(string $menu_current = null) {
        $this->response->data['menu'] = $this->getMenu();
        $this->response->data['menu_current'] = $menu_current;
        if ($this->response->hasUserMessages())
            $this->addUserMessage($this->getAndClearRedirectMessage());
    }

    protected function setView(string $view_name, bool $include_base = null) {
        $this->response->setView($view_name, $include_base);
    }

    protected function addUserMessage(string $message = null) {
        $this->response->addUserMessage($message);
    }

    protected function handleHTTPException(
        HTTPException $e, string $custom_data_field_name = null
    ) {
        $status_code = $e->getCode();
        if ($status_code >= 500)
            error_log(strval($e));
        $this->response->status_code = $status_code;
        $message = $e->getMessage();
        if ($custom_data_field_name === null)
            $this->addUserMessage($message);
        else
            $this->response->data[$custom_data_field_name] = $message;
    }
}
