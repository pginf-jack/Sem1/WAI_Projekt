<?php
namespace App\Lib;

abstract class CollectionModel {
    const _COLLECTION_NAME = null;
    protected $db;
    protected $collection;

    public function __construct() {
        $this->db = Database::getDB();
        if (static::_COLLECTION_NAME === null)
            throw new \TypeError('Subclass did not define collection name.');
        $this->collection = $this->db->selectCollection(static::_COLLECTION_NAME);
    }
}
