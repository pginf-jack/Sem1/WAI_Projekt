<?php
namespace App\Lib;

final class Config {
    const CONFIG_FILE = Utils::SRC_DIR . '/config.json';

    private static $config;

    public static function get(string $key, $default = null) {
        if (is_null(self::$config))
            self::$config = json_decode(file_get_contents(self::CONFIG_FILE), true);

        if (isset(self::$config[$key]))
            return self::$config[$key];

        return $default;
    }

    public static function set(string $key, $value) {
        self::$config[$key] = $value;
        file_put_contents(self::CONFIG_FILE, json_encode(self::$config, JSON_PRETTY_PRINT));
    }
}
