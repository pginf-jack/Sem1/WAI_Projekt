<?php
namespace App\Lib;

final class Cookie {
    public $name;
    public $value;
    public $expires;
    public $path;
    public $domain;
    public $secure;
    public $httponly;

    public function __construct(
        string $name,
        string $value = '',
        int $expires = 0,
        string $path = '',
        string $domain = '',
        bool $secure = false,
        bool $httponly = false
    ) {
        $this->name = $name;
        $this->value = $value;
        $this->expires = $expires;
        $this->path = $path;
        $this->domain = $domain;
        $this->secure = $secure;
        $this->httponly = $httponly;
    }

    public function getParameters(): array {
        return [
            $this->name,
            $this->value,
            $this->expires,
            $this->path,
            $this->domain,
            $this->secure,
            $this->httponly,
        ];
    }
}
