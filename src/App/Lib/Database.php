<?php
namespace App\Lib;

use MongoDB\Client as MongoDBClient;
use MongoDB\Database as MongoDBDatabase;

final class Database {
    private static $client = null;
    private static $db = null;

    private function __construct() {}

    public static function getClient(): MongoDBClient {
        if (static::$client === null) {
            $uri = 'mongodb://';
            $uri .= Config::get('db_host');
            $db_port = Config::get('db_port');
            if ($db_port)
                $uri .= ':' . $db_port;
            $uri .= '/' . Config::get('db_name');

            static::$client = new MongoDBClient(
                $uri,
                [
                    'username' => Config::get('db_username'),
                    'password' => Config::get('db_password'),
                ]
            );
        }

        return static::$client;
    }

    public static function getDB(): MongoDBDatabase {
        if (static::$db === null) {
            $client = static::getClient();
            static::$db = $client->selectDatabase(Config::get('db_name'));
        }

        return static::$db;
    }
}
