<?php
namespace App\Lib;

final class EventDispatcher {
    public $event_listeners;

    public function addListener(string $event_name, callable $callback) {
        if (!isset($this->event_listeners[$event_name]))
            $this->event_listeners[$event_name] = [];
        $this->event_listeners[$event_name][] = $callback;
    }

    public function dispatch(string $event_name, ...$args) {
        if (!isset($this->event_listeners[$event_name]))
            return;
        foreach ($this->event_listeners[$event_name] as $callback)
            $callback(...$args);
    }
}
