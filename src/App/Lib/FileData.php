<?php
namespace App\Lib;

final class FileData {
    const MIME_TYPE_EXTENSIONS = [
        'image/jpeg' => 'jpg',
        'image/png' => 'png',
    ];

    private $type;  // private because it can't be trusted either way
    private $mime_type = null;

    public $original_name;
    public $size;
    public $tmp_name;
    public $error;

    public function __construct(array $raw_file_data) {
        $this->original_name = $raw_file_data['name'];
        $this->type = $raw_file_data['type'];
        $this->size = $raw_file_data['size'];
        $this->tmp_name = $raw_file_data['tmp_name'];
        $this->error = $raw_file_data['error'];
    }

    public function getMIMEType() {
        if ($this->mime_type === null) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $this->mime_type = finfo_file($finfo, $this->tmp_name);
            finfo_close($finfo);
        }
        return $this->mime_type;
    }

    public function getFileExtension() {
        $mime_type = $this->getMIMEType();
        if (!isset(static::MIME_TYPE_EXTENSIONS[$mime_type]))
            return null;
        return static::MIME_TYPE_EXTENSIONS[$mime_type];
    }
}
