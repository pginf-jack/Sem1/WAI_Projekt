<?php
namespace App\Lib\Form;

use App\Lib\Form\Input;

class FormModel {
    public $inputs;
    public $name;
    public $action = null;
    public $method = 'post';

    public function __construct(array $options = []) {

    }

    public static function constructWithData(array $data, array $options = []) {
        $form_model = new static($options);
        $form_model->setData($data);
        return $form_model;
    }

    public function setName(string $name) {
        $this->name = $name;
    }

    public function setAction(string $url = null) {
        $this->action = $action;
    }

    public function setMethod(string $method) {
        $this->method = $method;
    }

    protected function addFileInput(array $options) {
        $input = new Input\FileInput($this, $options);
        $this->inputs[$input->name] = $input;
    }

    protected function addEmailInput(array $options) {
        $input = new Input\EmailInput($this, $options);
        $this->inputs[$input->name] = $input;
    }

    protected function addPasswordInput(array $options) {
        $input = new Input\PasswordInput($this, $options);
        $this->inputs[$input->name] = $input;
    }

    protected function addRadioGroupInput(array $options) {
        $input = new Input\RadioGroupInput($this, $options);
        $this->inputs[$input->name] = $input;
    }

    protected function addTextInput(array $options) {
        $input = new Input\TextInput($this, $options);
        $this->inputs[$input->name] = $input;
    }

    public function setData(array $data) {
        foreach ($data as $input_name => $input_data)
            if (isset($this->inputs[$input_name]))
                $this->inputs[$input_name]->setValue($input_data);
    }

    public function validate() {
        foreach ($this->inputs as $input)
            $input->validate();
    }
}
