<?php
namespace App\Lib\Form;

use App\Lib\Form\Input\Input;
use App\Lib\Utils;

class FormView {
    public $model;
    public $button_text = 'Wyślij';

    public function __construct(FormModel $form_model) {
        $this->model = $form_model;
    }

    public function setButtonText(string $text = null) {
        $this->button_text = $text;
    }

    protected function require_extract(string $_path, array &$_vars = []) {
        extract($_vars, EXTR_REFS);
        require $_path;
    }

    protected function view(
        string $name, array $vars = [], bool $require = true
    ): string {
        ob_start();
        $path = Utils::SRC_DIR . '/App/View/form/' . $name . '.php';
        if ($require || file_exists($path))
            $this->require_extract($path, $vars);
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    protected function renderValidators(Input $input): string {
        $output = '';
        foreach ($input->validators as $validator)
            $output .= $this->view(
                Utils::getShortClassName($validator),
                ['input' => $input, 'validator' => $validator],
                false
            );
        return $output;
    }

    protected function renderInputs(): string {
        $output = '';
        foreach ($this->model->inputs as $input)
            $output .= $this->view(
                Utils::getShortClassName($input),
                ['input' => $input]
            );
        return $output;
    }

    public function render(): string {
        return $this->view('base');
    }
}
