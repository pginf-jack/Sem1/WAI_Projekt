<?php
namespace App\Lib\Form\Input;

use App\Exception\InvalidTypeValidationException;
use App\Exception\MultipleFilesUnsupportedValidationException;
use App\Lib\FileData;

final class FileInput extends Input {
    public function getTempFileName(int $index = 0) {
        if ($this->value === null)
            throw new \DomainException('Value is not set.');
        return $this->value[$index]->tmp_name;
    }

    public function getFileSize(int $index = 0) {
        if ($this->value === null)
            throw new \DomainException('Value is not set.');
        return $this->value[$index]->size;
    }

    public function getMIMEType(int $index = 0) {
        if ($this->value === null)
            throw new \DomainException('Value is not set.');
        return $this->value[$index]->getMIMEType();
    }

    public function getFileExtension(int $index = 0) {
        return $this->value[$index]->getFileExtension();
    }

    public function validate() {
        if ($this->value !== null && !is_array($this->value))
            throw new InvalidTypeValidationException(
                "Pole $this->title musi być plikiem."
            );

        if ($count = count($this->value)) {
            if (!($this->value[0] instanceof FileData))
                throw new InvalidTypeValidationException(
                    "Pole $this->title musi być plikiem."
                );

            if ($count > 1)
                throw new MultipleFilesUnsupportedValidationException(
                    "Pole $this->title akceptuje tylko jeden plik."
                );
        }

        parent::validate();
    }
}
