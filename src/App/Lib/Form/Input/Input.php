<?php
namespace App\Lib\Form\Input;

use App\Exception\RequiredInputMissingValidationException;
use App\Lib\Form\FormModel;

abstract class Input {
    protected $value = null;
    private $metadata;

    public $form_model;
    public $name;
    public $title;
    public $help = null;
    public $required = false;
    public $validators = [];

    public function __construct(FormModel $form_model, array $options) {
        $this->form_model = $form_model;
        $this->name = $options['name'];
        $this->title = $options['title'];
        if (isset($options['help']))
            $this->help = $options['help'];
        if (isset($options['required']))
            $this->required = $options['required'];
        if (isset($options['validators']))
            $this->validators = $options['validators'];
    }

    public function getMetadata(string $key) {
        if (isset($this->metadata[$key]))
            return $this->metadata[$key];
        throw new \DomainException('There is no metadata set for given key.');
    }

    public function setMetadata(string $key, $value) {
        $this->metadata[$key] = $value;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function validate() {
        if ($this->value === null) {
            if ($this->required)
                throw new RequiredInputMissingValidationException(
                    "Wymagane pole ($this->title) nie zostało podane."
                );
            return;
        }
        foreach ($this->validators as $validator) {
            $validator->validate($this);
        }
    }
}
