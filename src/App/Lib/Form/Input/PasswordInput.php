<?php
namespace App\Lib\Form\Input;

use App\Exception\InvalidTypeValidationException;
use App\Lib\Form\FormModel;

final class PasswordInput extends Input {
    public $placeholder = null;

    public function __construct(FormModel $form_model, array $options) {
        parent::__construct($form_model, $options);
        if (isset($options['placeholder']))
            $this->placeholder = $options['placeholder'];
    }

    public function validate() {
        if ($this->value !== null && !is_string($this->value))
            throw new InvalidTypeValidationException(
                "Pole $this->title musi być ciągiem znaków."
            );
        parent::validate();
    }
}
