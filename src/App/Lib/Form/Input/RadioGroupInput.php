<?php
namespace App\Lib\Form\Input;

use App\Exception\InvalidRadioValueValidationException;
use App\Lib\Form\FormModel;

final class RadioGroupInput extends Input {
    public $options;
    public $default = null;
    public $allowed_options = [];

    public function __construct(FormModel $form_model, array $options) {
        parent::__construct($form_model, $options);
        $this->options = $options['options'];
        if (isset($options['default']))
            $this->default = $options['default'];
        foreach ($this->options as $option)
            $this->allowed_options[] = $option['value'];
    }

    public function getValue() {
        if ($this->value === null)
            return $this->options[$this->default]['value'];
        return $this->value;
    }

    public function validate() {
        if ($this->value !== null && !in_array($this->value, $this->allowed_options))
            throw new InvalidRadioValueValidationException(
                "Pole wyboru $this->title ma nieprawidłową wartość."
            );

        parent::validate();
    }
}
