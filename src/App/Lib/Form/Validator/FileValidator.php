<?php
namespace App\Lib\Form\Validator;

use App\Exception\IllegalFileTypeValidationException;
use App\Exception\MaxFileSizeExceededValidationException;
use App\Lib\Form\Input\Input;

class FileValidator extends Validator {
    public $allowed_types = null;
    public $max_size = null;

    public function __construct(array $options) {
        if (isset($options['allowed_types']))
            $this->allowed_types = $options['allowed_types'];
        if (isset($options['max_size']))
            $this->max_size = $options['max_size'];
    }

    public function validate(Input $input) {
        if ($this->max_size && $input->getFileSize() > $this->max_size)
            throw new MaxFileSizeExceededValidationException(
                "Wybrany plik (pole $input->title) przekracza dozwolony limit wielkości."
            );

        if (!in_array($input->getMIMEType(), $this->allowed_types))
            throw new IllegalFileTypeValidationException(
                "Wybrany plik (pole $input->title) nie używa żadnego"
                . ' z dozwolonych formatów (' . implode(', ', $this->allowed_types) . ').'
            );
    }
}
