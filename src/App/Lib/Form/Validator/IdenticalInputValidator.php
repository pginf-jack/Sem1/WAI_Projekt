<?php
namespace App\Lib\Form\Validator;

use App\Exception\DifferentValuesValidationException;
use App\Lib\Form\Input\Input;

class IdenticalInputValidator extends Validator {
    public $input_name;

    public function __construct(array $options) {
        $this->input_name = $options['input_name'];
    }

    public function validate(Input $input) {
        $other = $input->form_model->inputs[$this->input_name];
        if ($input->getValue() !== $other->getValue())
            throw new DifferentValuesValidationException(
                "Pola $other->title i $input->title muszą mieć identyczne wartości."
            );
    }
}
