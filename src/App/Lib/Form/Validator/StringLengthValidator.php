<?php
namespace App\Lib\Form\Validator;

use App\Exception\InvalidStringLengthValidationException;
use App\Lib\Form\Input\Input;

class StringLengthValidator extends Validator {
    public $min = null;
    public $max = null;

    public function __construct(array $options) {
        if (isset($options['min']))
            $this->min = $options['min'];
        if (isset($options['max']))
            $this->max = $options['max'];
    }

    public function validate(Input $input) {
        $value = $input->getValue();
        $length = strlen($value);
        if ($this->min && $length < $this->min)
            throw new InvalidStringLengthValidationException(
                "Podany tekst (pole $input->title) nie może być krótszy niż $this->min znaków."
            );
        if ($this->max && $length > $this->max)
            throw new InvalidStringLengthValidationException(
                "Podany tekst (pole $input->title) nie może być dłuższy niż $this->max znaków."
            );
    }
}
