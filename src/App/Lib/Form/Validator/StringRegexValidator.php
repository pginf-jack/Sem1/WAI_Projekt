<?php
namespace App\Lib\Form\Validator;

use App\Exception\PatternNotMatchedValidationException;
use App\Lib\Form\Input\Input;

class StringRegexValidator extends Validator {
    public $pattern;
    public $pattern_help = null;

    public function __construct(array $options) {
        $this->pattern = $options['pattern'];
        if (isset($options['pattern_help']))
            $this->pattern_help = $options['pattern_help'];
    }

    private function getPatternWithDelimiter() {
        return '/' . str_replace('/', '\/', $this->pattern) . '/';
    }

    public function validate(Input $input) {
        if (!preg_match($this->getPatternWithDelimiter(), $input->getValue())) {
            $additional_text = $this->pattern_help === null ? '' : " $this->pattern_help";
            throw new PatternNotMatchedValidationException(
                "Podany tekst (pole $input->title) nie spełnia wymaganego formatu."
                . $additional_text
            );
        }
    }
}
