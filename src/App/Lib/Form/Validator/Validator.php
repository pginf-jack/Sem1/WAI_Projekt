<?php
namespace App\Lib\Form\Validator;

use App\Lib\Form\Input\Input;

abstract class Validator {
    abstract public function validate(Input $input);
}
