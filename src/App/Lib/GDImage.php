<?php
namespace App\Lib;

use App\Exception\RuntimeException;

class GDImage {
    private $im;
    public $width;
    public $height;

    private function __construct($im) {
        $this->im = $im;
        $this->width = imagesx($im);
        $this->height = imagesy($im);
    }

    private static function getImageFunctionSuffix(string $path) {
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        switch ($extension) {
            case 'png':
                return 'png';
            case 'jpg':
                return 'jpeg';
            default:
                throw new \TypeError('Image uses unsupported file extension.');
        }
    }

    public static function new(int $width, int $height): GDImage {
        return new GDImage(imagecreatetruecolor($width, $height));
    }

    public static function open(string $path): GDImage {
        $func = 'imagecreatefrom' . static::getImageFunctionSuffix($path);
        $im = @$func($path);
        if (!$im)
            throw new RuntimeException('Unable to open image ' . $path);
        return new GDImage($im);
    }

    public function save(string $path) {
        $func = 'image' . static::getImageFunctionSuffix($path);
        if (!$func($this->im, $path))
            throw new RuntimeException('Unable to save image to ' . $path);
    }

    /**
     * Add text to this image.
     *
     * @param array $coords Coordinates array in form of [x, y] for this image.
     * @param string $text Text to add.
     * @param array $fill Color of the text fill in form of [r, g, b].
     * @param string $font_path Font to use.
     * @param int $font_size Font size.
     * @param int $stroke_width Stroke width, use 0 to disable stroke.
     * @param array $stroke Color of the stroke in form of [r, g, b]. Stroke will use $fill when this is `null`.
     */
    public function addText(
        array $coords,
        string $text,
        array $fill,
        string $font_path,
        int $font_size,
        int $stroke_width = 0,
        array $stroke = null
    ) {
        $offsets = $this->imagettfbbox_exc($font_size, 0, $font_path, $text);
        list($x, $y) = [$coords[0] - $offsets[6], $coords[1] - $offsets[7]];
        $fill = $this->getColor(...$fill);

        if ($stroke_width) {
            $stroke = $stroke ? $this->getColor(...$stroke) : $fill;
            for ($sx = $x-$stroke_width; $sx <= $x+$stroke_width; $sx++)
                for ($sy = $y-$stroke_width; $sy <= $y+$stroke_width; $sy++)
                    $this->imagettftext_exc(
                        $font_size, 0, $sx, $sy, $stroke, $font_path, $text
                    );
        }

        $this->imagettftext_exc($font_size, 0, $x, $y, $fill, $font_path, $text);
    }

    private function imagettftext_exc($size, $angle, $x, $y, $color, $fontfile, $text) {
        Utils::throwOnError(
            E_WARNING,
            function () use ($size, $angle, $x, $y, $color, $fontfile, $text) {
                $success = imagettftext(
                    $this->im, $size, $angle, $x, $y, $color, $fontfile, $text
                );
                // any failure should result in an error,
                // so this should technically never be reached
                if (!$success)
                    throw new RuntimeException('Unexpected error occured when writing text to image.');
            }
        );
    }

    private function imagettfbbox_exc($size, $angle, $fontfile, $text) {
        return Utils::throwOnError(
            E_WARNING,
            function () use ($size, $angle, $fontfile, $text) {
                $ret = imagettfbbox($size, $angle, $fontfile, $text);
                // any failure should result in an error,
                // so this should technically never be reached
                if (!$ret)
                    throw new RuntimeException(
                        'Unexpected error occured when generating bounding box of a text.'
                    );
                return $ret;
            }
        );
    }

    private function getColor(int $red, int $green, int $blue): int {
        $color = imagecolorallocate($this->im, $red, $green, $blue);
        if ($color === false)
            throw new RuntimeException('Color allocation for ' . json_encode($fill) . ' failed.');
        return $color;
    }

    public function generateThumbnail(int $width, int $height): GDImage {
        $dest = GDImage::new($width, $height);
        $this->copyResampled(
            $dest,
            [[0, 0], [$this->width, $this->height]],
            [[0, 0], [$width, $height]]
        );
        return $dest;
    }

    /**
     * Copy and resize part of this image to $dest with resampling.
     *
     * @param GDImage &$dest Destination image.
     * @param array $src_coords Coordinates array in form of [[x1, y1], [x2, y2]] for this image.
     * @param array $dest_coords Coordinates array in form of [[x1, y1], [x2, y2]] for destination image.
     */
    public function copyResampled(GDImage $dest, array $src_coords, array $dest_coords) {
        list($dst_x, $dst_y) = $dest_coords[0];
        $dst_w = $dest_coords[1][0] - $dst_x;
        $dst_h = $dest_coords[1][1] - $dst_y;
        list($src_x, $src_y) = $src_coords[0];
        $src_w = $src_coords[1][0] - $src_x;
        $src_h = $src_coords[1][1] - $src_y;

        imagecopyresampled(
            $dest->im, $this->im, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h
        );
    }
}
