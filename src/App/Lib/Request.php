<?php
namespace App\Lib;

require_once Utils::SRC_DIR . '/App/Lib/constants.php';

final class Request {
    public $method;
    public $route_uri;
    public $headers;
    public $query_params;
    public $post_data;
    public $files;
    public $form_data;
    public $cookies;

    public function __construct() {
        // Documentation doesn't say if REQUEST_METHOD is always uppercase,
        // so let's ensure it's standardized
        $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
        $this->query_params = $_GET;
        $this->post_data = $_POST;
        $this->cookies = $_COOKIE;
        $this->route_uri = $this->query_params['route'];
        $this->populateFiles();
        $this->populateFormData();
        $this->populateHeaders();
    }

    private function populateFiles() {
        $raw_files = [];
        foreach ($_FILES as $input_name => $file_or_files) {
            if (!is_array($file_or_files['error'])) {
                $raw_files[$input_name] = [$file_or_files];
                continue;
            }

            $raw_files[$input_name] = [];
            foreach ($file_or_files as $key => $values) {
                foreach ($values as $index => $value) {
                    if (!isset($raw_files[$input_name][$index]))
                        $raw_files[$input_name][$index] = [];

                    $raw_files[$input_name][$index][$key] = $value;
                }
            }
        }

        $this->files = [];
        foreach ($raw_files as $input_name => $files) {
            $this->files[$input_name] = [];
            foreach ($files as $raw_file_data) {
                $this->files[$input_name][] = new FileData($raw_file_data);
            }
        }
    }

    private function populateFormData() {
        $this->form_data = array_merge($this->post_data, $this->files);
    }

    private function populateHeaders() {
        if(!empty($_SERVER['CONTENT_TYPE']))
            $this->headers[HTTP_HEADER_CONTENT_TYPE] = $_SERVER['CONTENT_TYPE'];

        foreach ($_SERVER as $key => $value) {
            if (sscanf($key, 'HTTP_%s', $header_name)) {
                $header_name = str_replace('_', '-', strtolower($header_name));
                $this->headers[$header_name] = $value;
            }
        }
    }

    public function getRefererPath() {
        if (!isset($this->headers[HTTP_HEADER_REFERER]))
            return null;

        $path_components = parse_url($this->headers[HTTP_HEADER_REFERER]);
        if (!$path_components)
            return null;

        if (!isset($path_components['host']) || $_SERVER['SERVER_ADDR'] !== $path_components['host'])
            return null;

        if (isset($path_components['port']) && $_SERVER['SERVER_PORT'] !== strval($path_components['port']))
            return null;

        if (!isset($path_components['path']))
            return '/';
        return $path_components['path'];
    }
}
