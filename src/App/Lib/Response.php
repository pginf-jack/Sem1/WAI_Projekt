<?php
namespace App\Lib;

use App\Exception\RouteNotFoundException;

require_once Utils::SRC_DIR . '/App/Lib/constants.php';

final class Response {
    public $app_state;
    public $router;

    public $status_code = 200;
    public $body = '';
    public $headers = [];
    public $cookies = [];
    public $data = [
        'user_messages' => []
    ];
    public $view_name = null;
    public $view_include_base = true;

    public function __construct(AppState $app_state, Router $router) {
        $this->app_state = $app_state;
        $this->router = $router;
        $this->data['app_state'] = $app_state;
    }

    public function setView(string $view_name, bool $include_base = null) {
        $this->view_name = $view_name;
        if ($include_base !== null)
            $this->view_include_base = $include_base;
    }

    public function hasUserMessages(): bool {
        return !$this->data['user_messages'];
    }

    public function addUserMessage(string $user_message = null) {
        if ($user_message !== null)
            $this->data['user_messages'][] = $user_message;
    }

    public function addCookie(...$args) {
        $cookies[] = Cookie(...$args);
    }

    public function removeCookie(string $name, string $path = '', string $domain = '') {
        $cookies[] = Cookie($name, '', 1, $path, $domain);
    }

    public function redirectBackWithMessage(string $message) {
        $this->redirectBack();
        $_SESSION['redirect_message'] = $message;
    }

    public function redirectBack() {
        $referer_path = $this->app_state->request->getRefererPath();
        if ($referer_path === null)
            throw new RouteNotFoundException('Referer route could not be found.');
        list($route, $args) = $this->router->getMatchingRouteAndArguments(
            HTTP_METHOD_GET, $referer_path
        );
        $this->redirect($route->controller, $route->action, ...$args);
    }

    public function redirectWithMessage(
        string $message, string $controller, string $action, ...$args
    ) {
        $this->redirect($controller, $action, ...$args);
        $_SESSION['redirect_message'] = $message;
    }

    public function redirect(string $controller, string $action, ...$args) {
        $redirect_url = $this->router->routeTo(HTTP_METHOD_GET, $controller, $action);
        if ($redirect_url === null)
            throw new \DomainException('Route with these parameters was not found.');
        $this->status_code = 303;
        $this->headers[HTTP_HEADER_LOCATION] = $redirect_url;
    }

    public function send() {
        $this->sendHeaders();
        $this->sendCookies();
        $this->sendBody();
    }

    private function sendHeaders() {
        foreach ($this->headers as $name => $value)
            header($name . ': ' . $value);
        http_response_code($this->status_code);
    }

    private function sendCookies() {
        foreach ($this->cookies as $cookie)
            setcookie(...$cookie->getParameters());
    }

    private function sendBody() {
        if ($this->view_name !== null) {
            $view = new View($this->view_name, $this->data, $this->view_include_base);
            $this->body .= $view->render();
        }
        echo $this->body;
    }
}
