<?php
namespace App\Lib;

final class Route {
    public $path;
    public $pattern;
    public $http_method;
    public $controller;
    public $action;
    public $arg_count;

    public function __construct(
        string $path, string $http_method, string $controller, string $action
    ) {
        $this->path = $path;
        $this->arg_count = substr_count($this->path, '$');
        $this->pattern = $this->generatePattern();
        $this->http_method = $http_method;
        $this->controller = $controller;
        $this->action = $action;
    }

    private function generatePattern(): string {
        return '(^' . $this->format(...array_fill(0, $this->arg_count, '(.+)')) . '$)';
    }

    public function format(...$args): string {
        return preg_replace_callback(
            '/\$(\d+)/',
            function ($matches) use ($args) {
                return $args[intval($matches[1])];
            },
            $this->path
        );
    }
}
