<?php
namespace App\Lib;

use App\Exception\RouteNotFoundException;

require_once Utils::SRC_DIR . '/App/Lib/constants.php';

final class Router {
    private $routes = [];

    public $acl = null;

    public function setACLRules(ACLRules $acl) {
        $acl->validateRoutesExist($this);
        $this->acl = $acl;
    }

    public function hasPath(string $method, string $path): bool {
        foreach ($this->routes as $route) {
            if ($path != $route->path)
                continue;
            if ($method != $route->http_method)
                continue;
            return true;
        }
        return false;
    }

    public function routeTo(string $method, string $controller, string $action, ...$args) {
        foreach ($this->routes as $route) {
            if ($method != $route->http_method)
                continue;
            if ($route->controller !== $controller)
                continue;
            if ($route->action !== $action)
                continue;
            if ($route->arg_count !== count($args))
                continue;
            return $route->format(...$args);
        }
        return null;
    }

    private function register(Route $route) {
        $this->routes[] = $route;
    }

    public function get(string $path, string $controller, string $action) {
        $this->register(new Route($path, HTTP_METHOD_GET, $controller, $action));
    }

    public function post(string $path, string $controller, string $action) {
        $this->register(new Route($path, HTTP_METHOD_POST, $controller, $action));
    }

    public function getMatchingRouteAndArguments(string $method, string $route_uri): array {
        foreach ($this->routes as $route) {
            if ($method != $route->http_method)
                continue;

            if (preg_match($route->pattern, $route_uri, $matches)) {
                array_shift($matches);
                return [$route, $matches];
            }
        }

        throw new RouteNotFoundException('Matching route was not found.');
    }

    public function dispatch(Response $response) {
        $app_state = $response->app_state;
        $request = $app_state->request;

        list($route, $args) = $this->getMatchingRouteAndArguments(
            $request->method, $request->route_uri
        );

        $this->acl->validatePermissions($route, $app_state->user);
        $controller = new $route->controller($response);
        $controller->{$route->action}(...$args);
    }
}