<?php
namespace App\Lib;

use MongoDB\BSON\ObjectId;

use App\Exception\PasswordInvalidException;
use App\Model\UserCollectionModel;

final class User {
    public $id;
    public $email;
    public $username;
    public $password;

    public function __construct(
        ObjectId $id, string $email, string $email_lowered, string $username, string $password_hash
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->email_lowered = $email_lowered;
        $this->username = $username;
        $this->password_hash = $password_hash;
    }

    public static function constructFromSessionData() {
        if (!isset($_SESSION['user_id']))
            return null;
        $user_collection = new UserCollectionModel();
        return $user_collection->getById($_SESSION['user_id']);
    }

    public static function constructFromDocument(array $document): User {
        return new User(
            $document['_id'],
            $document['email'],
            $document['email_lowered'],
            $document['username'],
            $document['password_hash']
        );
    }

    public function getRole() {
        return ACLRules::ROLE_USER;
    }

    public function verifyPassword(string $password): bool {
        return password_verify($password, $this->password_hash);
    }

    public function validatePassword(string $password) {
        if (!$this->verifyPassword($password))
            throw new PasswordInvalidException('Password does not match.');
    }
}
