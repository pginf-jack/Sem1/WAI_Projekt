<?php
namespace App\Lib;

use App\Exception\RuntimeException;

/* This class serves merely as a namespace,
because there's no function nor constant autoloading in PHP */
class Utils {
    const SRC_DIR = __DIR__ . '/../..';
    const PUBLIC_DIR = self::SRC_DIR . '/web';

    public static function str_starts_with(string $haystack, string $needle): bool {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }

    public static function getShortClassName($obj) {
        $qualified_name = get_class($obj);
        return substr($qualified_name, strrpos($qualified_name, '\\') + 1);
    }

    /**
     * Convenience function that sets error handler raising RuntimeException for errors
     * with error types specified in $error_types that occur while the $callback function is called.
     */
    public static function throwOnError(int $error_types, callable $callback) {
        set_error_handler(
            function ($errno, $errstr, $errfile, $errline) {
                // we don't want to throw for expressions prepended with `@`
                if (error_reporting() !== 0)
                    throw new RuntimeException(
                        $errstr,
                        $errno,
                        new \ErrorException($errstr, 0, $errno, $errfile, $errline)
                    );
                return false;
            },
            $error_types
        );
        try {
            return $callback();
        } finally {
            restore_error_handler();
        }
    }
}
