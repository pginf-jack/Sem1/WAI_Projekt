<?php
namespace App\Lib;

final class View {
    private static $views_metadata;
    const PUBLIC_DIR = Utils::PUBLIC_DIR;

    public $name;
    public $data;
    public $include_base = true;
    public $metadata;

    public function __construct(string $name, array $data, bool $include_base = true) {
        $this->name = $name;
        $this->data = $data;
        $this->include_base = $include_base;
        $this->metadata = $this->getMetadata();
    }

    private function getMetadata() {
        if (is_null(self::$views_metadata))
            self::$views_metadata = require Utils::SRC_DIR . '/App/View/metadata.php';
        if (isset(self::$views_metadata[$this->name]))
            return self::$views_metadata[$this->name];
        return [];
    }

    protected function getTitle(): string {
        return $this->metadata['title'];
    }

    protected function view(string $name): string {
        ob_start();
        require Utils::SRC_DIR . '/App/View/' . $name . '.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function render(): string {
        $output = '';
        if ($this->include_base)
            $output .= $this->view('base/header');
        $output .= $this->view($this->name);
        if ($this->include_base)
            $output .= $this->view('base/footer');
        return $output;
    }
}
