<?php
namespace App\Lib;

const HTTP_HEADER_CONTENT_TYPE = 'content-type';
const HTTP_HEADER_LOCATION = 'location';
const HTTP_HEADER_REFERER = 'referer';
const HTTP_METHOD_GET = 'GET';
const HTTP_METHOD_POST = 'POST';
