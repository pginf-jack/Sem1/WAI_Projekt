<?php
namespace App\Model;

use MongoDB\BSON\ObjectId;
use App\Lib\Utils;
use App\Lib\GDImage;

final class Image {
    const URL_PATH = '/images';
    const URL_PATH_THUMBNAILS = '/images/thumbnails';
    const URL_PATH_WATERMARKED = '/images/watermarked';
    const DIR = Utils::PUBLIC_DIR . self::URL_PATH;
    const THUMBNAILS_DIR = Utils::PUBLIC_DIR . self::URL_PATH_THUMBNAILS;
    const WATERMARKED_DIR = Utils::PUBLIC_DIR . self::URL_PATH_WATERMARKED;
    const TYPE_ORIGINAL = 0;
    const TYPE_THUMBNAIL = 1;
    const TYPE_WATERMARKED = 2;

    public $id;
    public $extension;
    public $title;
    public $author;
    public $public;
    public $user_id;

    public function __construct(
        ObjectId $id,
        string $extension,
        string $title = null,
        string $author = null,
        bool $public = true,
        ObjectId $user_id = null
    ) {
        $this->id = $id;
        $this->extension = $extension;
        $this->title = $title;
        $this->author = $author;
        $this->public = $public;
        $this->user_id = $user_id;
    }

    public static function constructFromDocument(array $document): Image {
        return new Image(
            $document['_id'],
            $document['extension'],
            $document['title'],
            $document['author'],
            $document['public'],
            $document['user_id']
        );
    }

    public function getFileName(): string {
        return $this->id . '.' . $this->extension;
    }

    public function getPath(int $type = self::TYPE_ORIGINAL): string {
        $base_path = '/' . $this->getFileName();
        switch ($type) {
            case self::TYPE_ORIGINAL:
                return static::DIR . $base_path;
            case self::TYPE_THUMBNAIL:
                return static::THUMBNAILS_DIR . $base_path;
            case self::TYPE_WATERMARKED:
                return static::WATERMARKED_DIR . $base_path;
            default:
                throw new \DomainException('Unsupported type passed.');
        }
    }

    public function getPublicPath(int $type = self::TYPE_ORIGINAL): string {
        $base_path = '/' . $this->getFileName();
        switch ($type) {
            case self::TYPE_ORIGINAL:
                return static::URL_PATH . $base_path;
            case self::TYPE_THUMBNAIL:
                return static::URL_PATH_THUMBNAILS . $base_path;
            case self::TYPE_WATERMARKED:
                return static::URL_PATH_WATERMARKED . $base_path;
            default:
                throw new \DomainException('Unsupported type passed.');
        }
    }

    public function generateImageWithWatermark(string $text) {
        $image = GDImage::open($this->getPath());
        $image->addText(
            [20, 20],
            $text,
            [255, 255, 255],
            Utils::SRC_DIR . '/fonts/arial.ttf',
            28,
            3,
            [0, 0, 0]
        );
        $image->save($this->getPath(self::TYPE_WATERMARKED));
    }

    public function generateThumbnail(int $width, int $height) {
        $image = GDImage::open($this->getPath());
        $thumbnail = $image->generateThumbnail($width, $height);
        $thumbnail->save($this->getPath(self::TYPE_THUMBNAIL));
    }

    public function deleteFiles() {
        $types = [self::TYPE_ORIGINAL, self::TYPE_THUMBNAIL, self::TYPE_WATERMARKED];
        foreach ($types as $type) {
            $path = $this->getPath($type);
            if (file_exists($path))
                unlink($path);
        }
    }
}
