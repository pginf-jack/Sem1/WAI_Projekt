<?php
namespace App\Model;

use MongoDB\BSON\ObjectId;

use App\Exception\RuntimeException;
use App\Lib\CollectionModel;
use App\Lib\User;

/**
 * Collection format:
 * - _id (ObjectId) - Image ID, also filename without extension
 * - title (string) - Image title
 * - author (string) - Image author
 * - user_id (ObjectId?) - User that uploaded the image or `null` if it was uploaded by a guest
 * - public (bool) - true if image should be visible publically, false otherwise
 * - extension (string) - Image extension
 */
final class ImageCollectionModel extends CollectionModel {
    const _COLLECTION_NAME = 'images';

    public function add(
        string $tmp_filename,
        string $extension,
        string $watermark_text,
        string $title,
        string $author,
        User $user = null,
        bool $public = true
    ): Image {
        $user_id = $user === null ? null : $user->id;
        // ObjectId is generated before inserting to ensure that we successfully save image
        // and generate everything else before inserting the record
        $document = [
            '_id' => new ObjectId(),
            'extension' => $extension,
            'title' => $title,
            'author' => $author,
            'user_id' => $user_id,
            'public' => $public
        ];
        $image = Image::constructFromDocument($document);

        try {
            if (!rename($tmp_filename, $image->getPath()))
                throw new RuntimeException('Moving uploaded file failed.');

            $image->generateImageWithWatermark($watermark_text);
            $image->generateThumbnail(200, 125);

            $this->collection->insertOne($document);
        } catch (\Throwable $e) {
            $image->deleteFiles();
            throw $e;
        }
        return $image;
    }

    protected function getPageCount(int $page_limit, array $filter): int {
        return ceil($this->collection->count($filter) / $page_limit);
    }

    private function getFilter(User $user = null, bool $show_others = true): array {
        if ($user === null)
            return [
                'public' => true,
            ];
        elseif ($show_others)
            return [
                '$or' => [
                    ['public' => true],
                    ['public' => false, 'user_id' => $user->id],
                ],
            ];
        else
            return [
                'user_id' => $user->id,
            ];
    }

    public function getPage(
        int $page_number, int $page_limit, User $user = null, bool $show_others = true
    ): ImagePage {
        $filter = $this->getFilter($user, $show_others);
        return $this->getPageWithFilter($filter, $page_number, $page_limit);
    }

    protected function getPageWithFilter(
        array $filter,
        int $page_number,
        int $page_limit
    ): ImagePage {
        $documents = $this->collection->find(
            $filter,
            [
                'skip' => ($page_number - 1) * $page_limit,
                'limit' => $page_limit,
                'typeMap' => ['root' => 'array'],
            ]
        );
        $images = [];
        foreach ($documents as $document) {
            $images[] = Image::constructFromDocument($document);
        }
        return new ImagePage($images, $page_number, $this->getPageCount($page_limit, $filter));
    }

    public function searchByTitle(
        string $query,
        int $page_number,
        int $page_limit,
        User $user = null,
        bool $show_others = true
    ): ImagePage {
        $valid_images_filter = $this->getFilter($user, $show_others);
        return $this->getPageWithFilter(
            [
                '$and' => [
                    $valid_images_filter,
                    [
                        'title' => [
                            '$regex' => preg_quote($query),
                            '$options' => 'i',
                        ],
                    ],
                ],
            ],
            $page_number,
            $page_limit
        );
    }

    private function getIdsFilter(array $ids, User $user = null) {
        $valid_images_filter = $this->getFilter($user);
        return [
            '$and' => [
                $valid_images_filter,
                ['_id' => ['$in' => $ids]],
            ],
        ];
    }

    public function getPageWithIds(array $ids, User $user = null) {
        $filter = $this->getIdsFilter($ids, $user);

        $documents = $this->collection->find($filter, ['typeMap' => ['root' => 'array']]);
        $images = [];
        foreach ($documents as $document) {
            $images[] = Image::constructFromDocument($document);
        }

        return new ImagePage($images, 1, $images ? 1 : 0);
    }

    public function getUnavailableIds(array $ids, User $user = null) {
        $ids = array_unique($ids);
        $filter = $this->getIdsFilter($ids, $user);

        $available_ids = $this->collection->distinct('_id', $filter);

        return array_diff($ids, $available_ids);
    }
}
