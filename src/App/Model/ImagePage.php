<?php
namespace App\Model;

final class ImagePage {
    public $images;
    public $current_page;
    public $total_pages;

    public function __construct(array $images, int $current_page, int $total_pages) {
        $this->images = $images;
        $this->current_page = $current_page;
        $this->total_pages = $total_pages;
    }
}
