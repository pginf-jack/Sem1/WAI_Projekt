<?php
namespace App\Model;

use MongoDB\BSON\ObjectId;

use App\Exception\IdsNotFoundException;
use App\Exception\RuntimeException;
use App\Lib\AppState;
use App\Lib\CollectionModel;
use App\Lib\User;
use App\Model\ImageCollectionModel;

/**
 * Collection format:
 * - _id (ObjectId) - User ID
 * - image_ids (Array of ObjectId) - array of IDs of remembered images
 */
final class RememberedImagesCollectionModel extends CollectionModel {
    const _COLLECTION_NAME = 'remembered_images';

    public function __construct() {
        parent::__construct();
        $this->image_collection = new ImageCollectionModel();
    }

    public function transferImageIdsFromSessionToUser(User $user) {
        $this->moveSessionData($user);
    }

    private function moveSessionData(User $user) {
        $ids = $this->getImageIds(null, false);
        if ($ids)
            $this->addImageIds($ids, $user, false);
        unset($_SESSION['remembered_images']);
    }

    public function addImages(array $ids, User $user = null) {
        $unavailable_ids = $this->image_collection->getUnavailableIds($ids, $user);
        if ($unavailable_ids)
            throw new IdsNotFoundException($unavailable_ids);

        $this->addImageIds($ids, $user);
    }

    public function removeImages(array $ids, User $user = null) {
        $this->removeImageIds($ids, $user);
    }

    public function getImageIds(User $user = null, bool $auto_move_session_data = true) {
        if ($user == null) {
            if (!isset($_SESSION['remembered_images']))
                return [];
            return $_SESSION['remembered_images'];
        }

        if ($auto_move_session_data)
            $this->moveSessionData($user);
        $document = $this->collection->findOne(
            ['_id' => $user->id],
            ['typeMap' => ['root' => 'array']]
        );
        if ($document === null)
            return [];
        return $document['image_ids'];
    }

    public function getPage(User $user = null) {
        return $this->image_collection->getPageWithIds($this->getImageIds($user), $user);
    }

    private function addImageIds(array $ids, User $user = null, bool $auto_move_session_data = true) {
        if ($user == null) {
            $this->setImageIds(
                array_unique(
                    array_merge($this->getImageIds($user), $ids)
                ),
                $user
            );
            return;
        }

        if ($auto_move_session_data)
            $this->moveSessionData($user);
        $this->collection->updateOne(
            ['_id' => $user->id],
            [
                '$addToSet' => [
                    'image_ids' => [
                        '$each' => $ids,
                    ],
                ],
            ],
            ['upsert' => true]
        );
    }

    private function removeImageIds(array $ids, User $user = null) {
        if ($user == null) {
            $this->setImageIds(
                array_diff($this->getImageIds($user), $ids),
                $user
            );
            return;
        }

        $this->moveSessionData($user);
        $this->collection->updateOne(
            ['_id' => $user->id],
            [
                '$pullAll' => [
                    'image_ids' => $ids,
                ],
            ]
        );
    }

    private function setImageIds(array $ids, User $user = null) {
        if ($user == null) {
            $_SESSION['remembered_images'] = $ids;
            return;
        }

        $this->collection->updateOne(
            ['_id' => $user->id],
            [
                '$set' => [
                    'image_ids' => $ids,
                ],
            ],
            ['upsert' => true]
        );
    }
}
