<?php
namespace App\Model;

use MongoDB\BSON\ObjectId;
use MongoDB\Driver\Exception as MongoDBDriverException;

use App\Exception\PatternNotMatchedValidationException;
use App\Exception\UserAlreadyExistsException;
use App\Exception\UserNotFoundException;
use App\Lib\CollectionModel;
use App\Lib\Config;
use App\Lib\User;

/**
 * Collection format:
 * - _id (ObjectId) - User ID
 * - email (string) - User email
 * - email_lowered (string; unique index) - Lowercase version of email for unique indexing
 * - username (string, unique index) - Username
 * - password_hash (string) - User password hashed with PHP's \password_hash()
 * Collection version: 1
 */
final class UserCollectionModel extends CollectionModel {
    const _COLLECTION_NAME = 'users';
    const USERNAME_PATTERN = '^[\w\-]{4,32}$';

    public function __construct() {
        parent::__construct();
        $collection_version = Config::get('db_collection_users_version', 0);
        if ($collection_version == 0) {
            list($username_index, $email_lower_index) = $this->collection->createIndexes([
                ['key' => ['username' => 1], 'unique' => true],
                ['key' => ['email_lowered' => 1], 'unique' => true]
            ]);
            Config::set('db_collection_users_version', 1);
        }
    }

    public function getByLogin(string $username, string $password = null) {
        $document = $this->collection->findOne(
            ['username' => $username],
            ['typeMap' => ['root' => 'array']]
        );
        if ($document === null)
            throw new UserNotFoundException('User with given ID does not exist.');

        $user = User::constructFromDocument($document);
        if ($password !== null)
            $user->validatePassword($password);

        return $user;
    }

    public function getById(ObjectId $user_id) {
        $document = $this->collection->findOne(
            ['_id' => $user_id],
            ['typeMap' => ['root' => 'array']]
        );
        if ($document === null)
            throw new UserNotFoundException('User with given ID does not exist.');
        return User::constructFromDocument($document);
    }

    public function add(string $email, string $username, string $password) {
        $username = strtolower($username);
        if (!preg_match('/' . static::USERNAME_PATTERN . '/', $username))
            throw new PatternNotMatchedValidationException(
                'Username can only contain letters, digits, underscores and hyphens,'
                . ' and must be of length 4-32.'
            );
        // Validating email would be too complicated
        // and realistically we would try to send message to it on registration
        // and have mail server verify that it's valid this way.
        $document = [
            'email' => $email,
            // Only domain part of email is case-insensitive, so we can't use lower-cased email
            // as source of truth. We can however use it as unique index, because differing casing
            // for email users in one domain will realistically only be exploited by users.
            'email_lowered' => strtolower($email),
            'username' => $username,
            'password_hash' => password_hash($password, PASSWORD_DEFAULT),
        ];
        try {
            $document['_id'] = $this->collection->insertOne($document)->getInsertedId();
        } catch (MongoDBDriverException\BulkWriteException $e) {
            $write_errors = $e->getWriteResult()->getWriteErrors();
            if ($write_errors && $write_errors[0]->getCode() == 11000)
                throw new UserAlreadyExistsException(
                    'User with this username or email already exists.'
                );

            throw $e;
        }
        return User::constructFromDocument($document);
    }
}
