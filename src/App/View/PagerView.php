<?php
namespace App\View;

use App\Lib\Utils;

final class PagerView {
    const PAGE_PLACEHOLDER = '[PAGE_PLACEHOLDER]';

    protected $base_url;
    protected $current;
    protected $total;
    protected $link_count;
    protected $start;
    protected $end;

    public function __construct(string $base_url, int $current, int $total, int $link_count = 2) {
        $this->base_url = $base_url;
        $this->current = $current;
        $this->total = $total;
        $this->link_count = $link_count;
        $this->start = $this->getStart();
        $this->end = $this->getEnd();
    }

    protected function view(string $name): string {
        ob_start();
        require Utils::SRC_DIR . '/App/View/pager/' . $name . '.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function render(): string {
        return $this->view('base');
    }

    private function renderItems(): string {
        $output = '';
        $output .= $this->view('beginning');
        $output .= $this->view('main');
        $output .= $this->view('end');
        return $output;
    }

    private function getStart(): int {
        if ($this->current - $this->link_count > 0)
            return $this->current - $this->link_count;
        else
            return 1;
    }

    private function getEnd(): int {
        if ($this->current + $this->link_count < $this->total)
            return $this->current + $this->link_count;
        else
            return $this->total;
    }

    public function getLink(int $page_number = 1) {
        $result = $this->base_url;
        $pos = strpos($this->base_url, static::PAGE_PLACEHOLDER);
        if ($pos !== false) {
            if ($page_number == 1)
                $replacement = '';
            else
                $replacement = '/' . $page_number;
            $result = substr_replace(
                $this->base_url, $replacement, $pos, strlen(static::PAGE_PLACEHOLDER)
            );
        }
        return $result;
    }
}
