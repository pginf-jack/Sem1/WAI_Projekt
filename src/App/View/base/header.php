<?php
use App\Lib\Utils;

require_once Utils::SRC_DIR . '/App/Lib/constants.php';
use const App\Lib\HTTP_METHOD_GET;
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php
        if (file_exists(static::PUBLIC_DIR . '/assets/js/' . $this->name . '.bundle.js')):
        ?>
        <script async="async" src="/assets/js/<?= $this->name ?>.bundle.js"></script>
        <?php
        endif;
        ?>
        <link rel="stylesheet" href="/assets/css/jquery-ui.min.css" />
        <link rel="stylesheet" href="/assets/css/slick.css" />
        <link rel="stylesheet" href="/assets/css/slick-theme.css" />
        <link href="/assets/css/main.css" rel="stylesheet" />
        <link rel="icon" href="/assets/img/favicon.ico" />
        <title><?= $this->getTitle() ?> | Społeczność open-source</title>
    </head>
    <body>
        <header>
            <div class="inner">
                <a id="site-name" href="/">
                    <img src="/assets/img/logo.png" alt="" />
                    Społeczność open&#8209;source
                </a>
                <nav>
                    <ul>
                        <?php
                        foreach ($this->data['menu'] as $route_url => $menu_data):
                            $friendly_name = $menu_data['title'];
                            if (!$this->data['app_state']->canRunRoutePath(HTTP_METHOD_GET, '/' . $route_url))
                                continue;
                            if ($route_url !== null && $this->data['menu_current'] === $route_url)
                                $class = 'active';
                            else
                                $class = '';
                        ?>
                            <li><a href="/<?= $route_url ?>" class="<?= $class ?>"><?= $friendly_name ?></a></li>
                        <?php
                        endforeach;
                        ?>
                    </ul>
                </nav>
            </div>
        </header>
        <main>
            <section>
                <?php
                foreach ($this->data['user_messages'] as $user_message):
                ?>
                <strong><?= $user_message ?></strong>
                <?php
                endforeach;
                ?>