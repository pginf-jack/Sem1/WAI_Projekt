<?php
$label = $input->title;
if ($input->help)
    $label .= " ($input->help)";
?>
<div class="form-group">
    <label for="<?= $this->model->name ?>-form-<?= $input->name ?>"><?= $label ?></label>
    <input
        type="file"
        name="file"
        id="<?= $this->model->name ?>-form-file"
        <?php
        if ($input->required):
        ?>
        required="required"
        <?php
        endif;

        echo $this->renderValidators($input);
        ?>
        />
</div>