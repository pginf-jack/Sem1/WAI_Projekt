<?php
$label = $input->title;
if ($input->help)
    $label .= " ($input->help)";
?>
<div class="form-group">
    <label for="<?= $this->model->name ?>-form-<?= $input->name ?>"><?= $label ?></label>
    <input
        type="password"
        name="<?= $input->name; ?>"
        id="<?= $this->model->name ?>-form-<?= $input->name ?>"
        <?php
        if ($input->placeholder):
        ?>
        placeholder="<?= $input->placeholder ?>"
        <?php
        endif;

        if ($input->required):
        ?>
        required="required"
        <?php
        endif;
        
        echo $this->renderValidators($input);
        ?>
        />
</div>