<?php
$label = $input->title;
if ($input->help)
    $label .= " ($input->help)";
?>
<div class="form-group">
    <span class="label"><?= $label ?></span>
    <?php
    foreach ($input->options as $index => $option):
    ?>
        <div class="form-group form-checkbox">
            <input
                type="radio"
                name="<?= $input->name; ?>"
                value="<?= $option['value'] ?>"
                id="<?= $this->model->name ?>-form-<?= $input->name ?>-<?= $option['value'] ?>"
                <?php
                if ($input->required):
                ?>
                required="required"
                <?php
                endif;

                if ($input->getValue() === $option['value']):
                ?>
                checked="checked"
                <?php
                endif;
                
                echo $this->renderValidators($input);

                ?>
                />
            <label for="<?= $this->model->name ?>-form-<?= $input->name ?>-<?= $option['value'] ?>">
                <?= $option['title'] ?>
            </label>
        </div>
    <?php
    endforeach;
    ?>
</div>