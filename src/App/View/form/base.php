<form
    id="<?= $this->model->name ?>-form"
    <?php
    if ($this->model->action !== null):
    ?>
    action="<?= $this->model->action ?>"
    <?php
    endif;
    ?>
    method="<?= $this->model->method ?>"
    enctype="multipart/form-data">
    <?php
    echo $this->renderInputs();
    if ($this->button_text !== null):
    ?>
        <button><?= $this->button_text ?></button>
    <?php
    endif;
    ?>
</form>