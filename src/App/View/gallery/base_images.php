<?php
use App\View\PagerView;

$image_page = $this->data['image_page'];

$pager_view = new PagerView(
    $this->data['pager_base_url'],
    $image_page->current_page,
    $image_page->total_pages
);

if (!$image_page->images):
    if (!$image_page->total_pages):
?>
        <p>Brak zdjęć.</p>
<?php
    else:
?>
        <p>Numer strony jest nieprawidłowy. <a href="/<?= $pager_view->getLink() ?>">Wróć do pierwszej strony</a></p>
<?php
    endif;
else:
?>
<form id="gallery-select-form" action="/gallery/remembered" method="post">
    <input type="hidden" name="operation" value="<?= $this->data['form_operation'] ?>">
    <div class="gallery">
    <?php
        foreach ($image_page->images as $image):
    ?>
            <div class="gallery-image">
                <div class="gallery-image-thumbnail">
                    <a target="_blank" href="<?= $image->getPublicPath($image::TYPE_WATERMARKED) ?>">
                        <img src="<?= $image->getPublicPath($image::TYPE_THUMBNAIL) ?>" alt="<?= $image->title ?>" />
                    </a>
                    <div class="gallery-image-description">
                        <label for="gallery-select-<?= $image->id ?>"><?= $image->title ?></label>
                        <div style="text-align:center">
                            <input
                                type="checkbox"
                                id="gallery-select-<?= $image->id ?>"
                                name="selected_images[]"
                                value="<?= $image->id ?>"
                                <?php
                                if (isset($this->data['selected_images_ids'])):
                                    if (in_array($image->id, $this->data['selected_images_ids'])):
                                ?>
                                        checked="checked"
                                <?php
                                    endif;
                                endif;
                                ?>
                                />
                        </div>
                    </div>
                </div>
                <div class="gallery-image-metadata">
                    <div><strong>Autor:</strong> <?= $image->author ?></div>
                    <?php
                    if (!$image->public):
                    ?>
                        <div>Zdjęcie prywatne</div>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
    <?php
        endforeach;
    ?>
    </div>
    <div style="text-align:center;margin-bottom:1em">
        <button><?= $this->data['form_button_text'] ?></button>
    </div>
</form>
<?php
    if ($image_page->total_pages > 1)
        echo $pager_view->render();
endif;
?>
