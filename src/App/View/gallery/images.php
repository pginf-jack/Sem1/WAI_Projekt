<?php
use App\Lib\Utils;

require_once Utils::SRC_DIR . '/App/Lib/constants.php';
use const App\Lib\HTTP_METHOD_GET;
?>
<h1>Galeria</h1>
<?php
$this->data['form_operation'] = 'add';
$this->data['form_button_text'] = 'Zapamiętaj wybrane';
echo $this->view('gallery/base_images');
?>
<p><a href="/gallery/upload">Dodaj zdjęcie</a></p>
<?php
if ($this->data['app_state']->canRunRoutePath(HTTP_METHOD_GET, '/gallery/me')):
?>
<p><a href="/gallery/me">Twoje zdjęcia</a></p>
<?php
endif;
?>
<p><a href="/gallery/remembered">Zapamiętane zdjęcia</a></p>
<p><a href="/gallery/search">Wyszukiwarka zdjęć</a></p>