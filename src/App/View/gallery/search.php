<?php
use App\Lib\Form\FormView;

if ($this->include_base):
?>
    <h1>Wyszukiwarka zdjęć</h1>
    <?php
    $form_view = new FormView($this->data['form']);
    $form_view->setButtonText(null);
    echo $form_view->render();
    ?>
    <div id="gallery-search-results">
<?php
endif;

echo $this->view('gallery/api_search');

if ($this->include_base):
?>
    </div>
<?php
endif;
