<h1>Przesyłanie zdjęć</h1>
<?php
use App\Lib\Form\FormView;
$form_view = new FormView($this->data['form']);
$form_view->setButtonText('Prześlij zdjęcie');
echo $form_view->render();
