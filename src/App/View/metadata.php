<?php
return [
    'user/profile' => ['title' => 'Profil'],
    'user/login_form' => ['title' => 'Formularz logowania'],
    'user/registration_form' => ['title' => 'Formularz rejestracyjny'],
    'gallery/images' => ['title' => 'Galeria'],
    'gallery/search' => ['title' => 'Wyszukiwarka zdjęć'],
    'gallery/remembered_images' => ['title' => 'Zapamiętane zdjęcia'],
    'gallery/user_images' => ['title' => 'Twoje zdjęcia'],
    'gallery/upload_form' => ['title' => 'Przesyłanie zdjęć'],
    'pages/index' => ['title' => 'Strona główna'],
    'pages/howtostart' => ['title' => 'Jak zacząć?'],
    'pages/contact' => ['title' => 'Kontakt'],
    'errors/custom_message' => ['title' => 'Wystąpił błąd'],
    'errors/forbidden' => ['title' => 'Brak dostępu'],
    'errors/not_found' => ['title' => 'Strona nie istnieje'],
    'errors/server_error' => ['title' => 'Błąd serwera'],
];
