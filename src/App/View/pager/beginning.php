<?php
if ($this->current == 1):
?>
    <li class="pager-previous disabled"><span>&laquo;</span></li>
<?php
else:
?>
    <li class="pager-previous">
        <a href="<?= $this->getLink($this->current - 1) ?>">&laquo;</a>
    </li>
<?php
endif;

if ($this->start > 1):
?>
    <li class="pager-item"><a href="<?= $this->getLink() ?>">1</a></li>
    <?php
    if ($this->start > 2):
        if ($this->start - 1 == 2):
    ?>
            <li class="pager-item">
                <a href="<?= $this->getLink($this->start - 1) ?>"><?= $this->start - 1 ?></a>
            </li>
    <?php
        else:
    ?>
            <li class="pager-item disabled"><span>...</span></li>
    <?php
        endif;
    endif;
    ?>
<?php
endif;
