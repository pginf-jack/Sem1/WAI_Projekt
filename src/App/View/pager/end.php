<?php
if ($this->end < $this->total):
?>
    <?php
    if ($this->end < ($this->total - 1)):
        if ($this->total - $this->end == 2):
    ?>
            <li class="pager-item">
                <a href="<?= $this->getLink($this->total - 1) ?>"><?= $this->total - 1 ?></a>
            </li>
    <?php
        else:
    ?>
            <li class="pager-item disabled"><span>...</span></li>
    <?php
        endif;
    endif;
    ?>
    <li class="pager-item">
        <a href="<?= $this->getLink($this->total) ?>"><?= $this->total ?></a>
    </li>
<?php
endif;

if ($this->current == $this->total):
?>
    <li class="pager-next disabled"><span>&raquo;</span></li>
<?php
else:
?>
    <li class="pager-next">
        <a href="<?= $this->getLink($this->current + 1) ?>">&raquo;</a>
    </li>
<?php
endif;
