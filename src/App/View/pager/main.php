<?php
for ($i = $this->start; $i <= $this->end; $i++):
    if ($this->current == $i):
?>
        <li class="pager-current"><span><?= $i ?></span></li>
<?php
    else:
?>
        <li class="pager-item">
            <a href="<?= $this->getLink($i) ?>"><?= $i ?></a>
        </li>
<?php
    endif;
endfor;
