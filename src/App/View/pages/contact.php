<h1>Kontakt</h1>

<p>Skontaktuj się z nami i postaramy się znaleźć projekt open-source dla Ciebie!</p>

<form id="contact-form" method="post">
    <div class="form-group">
        <label for="contact-form-name">Imię i nazwisko</label>
        <input type="text" name="name" id="contact-form-name" placeholder="Jan Kowalski" required="required" />
    </div>
    <div class="form-group">
        <label for="contact-form-email">Email</label>
        <input type="email" name="email" id="contact-form-email" placeholder="example@example.com" required="required" />
    </div>
    <div class="form-group">
        <label for="contact-form-message">Opowiedz coś o sobie</label>
        <textarea name="message" id="contact-form-message" rows="8" cols="70"></textarea>
    </div>
    <div class="form-group form-sortable">
        <label>Wybierz języki które znasz i posortuj wg swoich preferencji przeciągając je w odpowiednie miejsce</label>
        <div>
            <strong>Języki które znasz</strong>
            <ul id="contact-form-preferred-languages-ul" class="contact-form-languages"></ul>
            <input id="contact-form-preferred-languages" name="preferred-languages" type="hidden" value="" />
        </div>
        <div>
            <strong>Języki których nie znasz</strong>
            <ul id="contact-form-unknown-languages-ul" class="contact-form-languages"></ul>
        </div>
    </div>
    <noscript>
        <div class="form-group">
            <label for="contact-form-manual-languages-list">Podaj języki, które znasz w kolejności zgodnej ze swoimi preferencjami</label>
            <textarea name="manual-languages-list" id="contact-form-manual-languages-list" rows="8" cols="70"></textarea>
        </div>
    </noscript>
    <div class="form-group form-checkbox">
        <input id="contact-form-accept" name="accept" type="checkbox" required="required" />
        <label for="contact-form-accept">Wyrażam zgodę na przetwarzanie moich danych osobowych.</label>
    </div>
    <button>Wyślij wiadomość</button>
</form>