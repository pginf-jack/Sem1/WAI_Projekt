<h1>O otwartych źródłach słów kilka...</h1>

<h2>Czym jest open source?</h2>
<p>Oryginalnie, termin open-source był używany odnosząc się do otwartego oprogramowania (OSS). Otwarte oprogramowanie to oprogramowanie, którego kod jest dostępny publicznie i każdy może go zobaczyć, zmodyfikować czy dystrybuować.</p>
<p>Obecnie open-source to znacznie więcej niż tylko otwarte oprogramowanie. Open-source tworzy pewnego rodzaju społeczność, w której wszyscy mogą się przyczynić do jej rozwinięcia. Otwartoźródłowe projekty, produkty czy inicjatywy opierają się na zasadach otwartej wymiany informacji, współpracy, czy programowaniu skupionym na społeczności danego projektu.</p>

<h2>Jakie są zalety otwartego oprogramowania?</h2>
<ul>
    <li><strong>Bezpieczeństwo</strong> - otwarte oprogramowanie jest bezpieczne mimo tego, że szczegóły co do tego jak działają w nich zabezpieczenia są powszechnie znane. Choć mogłoby się wydawać, że jest to niejako paradoks to w rzeczywistości dzięki temu, że społeczność ma możliwość sprawdzenia jak oprogramowanie jest napisane, to jest ona w stanie zauważyć potencjalne podatności i wspomóc w ten sposób projekt, a inna osoba ze społeczności może niedługo później załatać tę podatność.</li>
    <li><strong>Niezawodność</strong> - własnościowy kod jest zależny od pojedynczego autora, bądź firmy która kontroluje, by pozostał on zaktualizowany, załatany i działający. Kod otwartoźródłowy nie jest zależny od nikogo, każdy może pomóc rozwijać ten kod, nawet gdy oryginalny autor już się nim nie zajmuje.</li>
    <li><strong>Elastyczność</strong> - dzięki nacisku na modyfikację, możesz używać otwartoźródłowego kodu, by rozwiązywać problemy specyficzne dla twojej firmy. Nie jesteś ograniczony przed używaniem kodu w żaden sposób i możesz polegać na pomocy od społeczności, czy wzajemnej ocenie przy tworzeniu nowych rozwiązań</li>
</ul>

<h2>Jakie są wady otwartego oprogramowania?</h2>
<ul>
    <li><strong>Brak gwarancji wsparcia</strong> - open-source w dużej mierze polega na wolontariacie, społeczność pomaga go rozwijać w swoim własnym czasie i w związku z tym nie ma gwarancji, że jakiekolwiek problemy zostaną rozwiązane w odpowiednim czasie.</li>
    <li><strong>Brak funduszy</strong> - autorzy dużych projektów zazwyczaj nie zarabiają wiele na swoich otwartoźródłowych projektach. Choć społeczność może pomagać w formie donacji, to generalnie większość z nich pochodzi od indywidualnych osób zainteresowanych projektem i w związku z tym nie są to kwoty, którymi możnaby było zapłacić za czas jaki programista musi poświęcić na projekt. Część projektów ma swoje zastosowanie dla firm i w takim przypadku mogą one wspierać finansowo twórcę, ale jest to prawdą tylko dla garstki projektów.</li>
</ul>

<h2>Firmy rozwijające projekty open source</h2>
<div id="companies-carousel">
    <div class="carousel-item"><img src="assets/img/company-logos/microsoft.png" alt="Microsoft logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/google.png" alt="Google logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/redhat.png" alt="Red Hat logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/ibm.png" alt="IBM logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/intel.png" alt="Intel logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/amazon.png" alt="Amazon logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/sap.png" alt="SAP logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/facebook.png" alt="Facebook logo" /></div>
    <div class="carousel-item"><img src="assets/img/company-logos/adobe.png" alt="Adobe logo" /></div>
</div>