<h1>Formularz logowania</h1>
<?php
use App\Lib\Form\FormView;
$form_view = new FormView($this->data['form']);
$form_view->setButtonText('Zaloguj się');
echo $form_view->render();
?>
<p><a href="/user/register">Zarejestruj się</a></p>