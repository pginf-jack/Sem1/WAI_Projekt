<h1>Formularz rejestracyjny</h1>
<?php
use App\Lib\Form\FormView;
$form_view = new FormView($this->data['form']);
$form_view->setButtonText('Zarejestruj się');
echo $form_view->render();
