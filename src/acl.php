<?php
use App\Lib\ACLRules;
use App\Lib\Utils;

require_once Utils::SRC_DIR . '/App/Lib/constants.php';
use const App\Lib\HTTP_METHOD_GET;
use const App\Lib\HTTP_METHOD_POST;

$acl = new ACLRules([ACLRules::ROLE_GUEST, ACLRules::ROLE_USER]);
$acl->setRouteRules('/user/register', [HTTP_METHOD_GET, HTTP_METHOD_POST], [ACLRules::ROLE_GUEST]);
$acl->setRouteRules('/user/login', [HTTP_METHOD_GET, HTTP_METHOD_POST], [ACLRules::ROLE_GUEST]);
$acl->setRouteRules('/user/profile', [HTTP_METHOD_GET], [ACLRules::ROLE_USER]);
$acl->setRouteRules('/user/logout', [HTTP_METHOD_POST], [ACLRules::ROLE_USER]);

$acl->setRouteRules('/gallery/me/$0', [HTTP_METHOD_GET], [ACLRules::ROLE_USER]);
$acl->setRouteRules('/gallery/me', [HTTP_METHOD_GET], [ACLRules::ROLE_USER]);

return $acl;
