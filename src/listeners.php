<?php
use App\Controller;
use App\Lib\EventDispatcher;

$dispatcher = new EventDispatcher();
$dispatcher->addListener('login', Controller\GalleryController::class . '::loginListener');

return $dispatcher;
