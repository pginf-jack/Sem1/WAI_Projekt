<?php
return [
    '' => ['title' => 'Strona główna'],
    'gallery' => ['title' => 'Galeria'],
    'howtostart' => ['title' => 'Jak zacząć?'],
    'user/login' => ['title' => 'Zaloguj się'],
    'user/profile' => ['title' => 'Profil'],
    'contact' => ['title' => 'Kontakt'],
];
