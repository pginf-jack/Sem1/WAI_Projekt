<?php
use App\Controller;
use App\Lib\Route;
use App\Lib\Router;

$router = new Router();
/* User controller */
$router->post('/user/register', Controller\UserController::class, 'handleRegistrationForm');
$router->get('/user/register', Controller\UserController::class, 'showRegistrationForm');

$router->post('/user/login', Controller\UserController::class, 'handleLoginForm');
$router->get('/user/login', Controller\UserController::class, 'showLoginForm');

$router->get('/user/profile', Controller\UserController::class, 'showProfile');
$router->post('/user/logout', Controller\UserController::class, 'handleLogout');

/* Gallery controller */
$router->get('/api/gallery/search', Controller\GalleryController::class, 'handleSearchFormAPI');
$router->get('/gallery/search/$0', Controller\GalleryController::class, 'showSearchForm');
$router->get('/gallery/search', Controller\GalleryController::class, 'showSearchForm');

$router->post('/gallery/upload', Controller\GalleryController::class, 'handleUploadForm');
$router->get('/gallery/upload', Controller\GalleryController::class, 'showUploadForm');

$router->post('/gallery/remembered', Controller\GalleryController::class, 'addOrRemoveRememberedImages');
$router->get('/gallery/remembered', Controller\GalleryController::class, 'showRememberedImages');

$router->get('/gallery/me/$0', Controller\GalleryController::class, 'showUserImages');
$router->get('/gallery/me', Controller\GalleryController::class, 'showUserImages');

$router->get('/gallery/$0', Controller\GalleryController::class, 'showImages');
$router->get('/gallery', Controller\GalleryController::class, 'showImages');

/* Static page controller */
$router->get('/$0', Controller\StaticPageController::class, 'showPage');
$router->get('/', Controller\StaticPageController::class, 'showPage');

return $router;
