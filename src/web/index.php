<?php
require __DIR__ . '/../../vendor/autoload.php';

use App\Lib\App;
use App\Lib\Utils;

$routes = require Utils::SRC_DIR . '/routes.php';
$acl = require Utils::SRC_DIR . '/acl.php';
$listeners = require Utils::SRC_DIR . '/listeners.php';
$app = new App($routes, $acl, $listeners);
$app->run();
