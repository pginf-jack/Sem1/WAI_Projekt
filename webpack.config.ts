import glob from "glob";
import path from "path";
import webpack from "webpack";

const context = path.resolve(__dirname, "client_src");

const config: webpack.Configuration = {
    mode: "production",
    entry: () => {
        let files = glob.sync("./**/*.ts", {cwd: context});
        let entries: {
            [key: string]: string
        } = {};

        for (let file of files) {
            entries[file.substring(2, file.length - 3)] = file;
        }

        return entries;
    },
    devtool: "source-map",
    output: {
        path: path.resolve(__dirname, "src", "web", "assets", "js"),
        filename: "[name].bundle.js",
    },
    resolve: {
        extensions: [ ".ts", ".js" ],
    },
    module: {
        rules: [
            {
                test: /\.tsx?/,
                use: "ts-loader",
                exclude: /node_modules/,
            }
        ]
    },
    context: context,
};

export default config;
